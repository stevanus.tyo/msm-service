/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import com.bsm.TWS;
import static com.bsm.TWS.TWS_LOCATION;
import static com.bsm.TWS.TWS_SERVICE;
import java.net.URL;
import java.util.Date;
import java.util.List;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import org.json.me.JSONObject;
import pkg_lib.LibConfig;
import t24webservicesimpl.EnquiryAccountTWS;
import t24webservicesimpl.EnquiryAccountTWSResponse;
import t24webservicesimpl.EnquiryInput;
import t24webservicesimpl.EnquiryInputCollection;
import t24webservicesimpl.IDIEACCTBALTWSType;
import t24webservicesimpl.SuccessIndicator;
import t24webservicesimpl.T24WebServicesImplPort;
import t24webservicesimpl.WebRequestCommon;

/**
 *
 * @author Mukhlis Yani
 */
public class Class5 implements Runnable {
    
    public int index;
    public static String[] arrAcc = new String[] {"7095539941", "7095539976"};
    public static Service service;
    
    static {
        try {
            if (service == null) {
                URL wsdlURL = new URL(TWS_LOCATION + "?wsdl");
                System.out.println("Start - Creating service: " + new Date());
                service = Service.create(wsdlURL, TWS_SERVICE);
                System.out.println("End - Creating service: " + new Date());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public JSONObject retrieve(JSONObject params) throws Exception {
        //System.out.println("Making connection to: " + TWS_LOCATION);
//        URL wsdlURL = new URL(TWS_LOCATION + "?wsdl");
//        Service service = Service.create(wsdlURL, TWS_SERVICE);
        T24WebServicesImplPort port = service.getPort(T24WebServicesImplPort.class);
        //((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://10.243.128.201:10109/ws/mdr.ubpconsumer.ws.inquiry/mdr_ubpconsumer_ws_inquiry_Port");
        //Set timeout until a connection is established
        //((BindingProvider) port).getRequestContext().put("com.sun.xml.internal.ws.connect.timeout", 5000);
        //System.out.println(((BindingProvider) port).getRequestContext().get("com.sun.xml.internal.ws.connect.timeout"));
        //Set timeout until the response is received
        ((BindingProvider) port).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", params.getInt("inq_timeout"));

        System.out.println("Preparing Account Retrieval parameters...");
        EnquiryAccountTWS account = new EnquiryAccountTWS();

        WebRequestCommon wrc = new WebRequestCommon();
        wrc.setUserName(params.getString("username_tws"));
        wrc.setPassword(params.getString("password_tws"));
        //wrc.setCompany(params.getString("company"));
        account.setWebRequestCommon(wrc);
        
        EnquiryInputCollection eic = new EnquiryInputCollection();
        eic.setColumnName("@ID");
        eic.setCriteriaValue(params.getString("account"));
        eic.setOperand("EQ");

        EnquiryInput ei = new EnquiryInput();
        ei.getEnquiryInputCollection().add(eic);
        
        account.setIDIEACCTBALTWSType(ei);
        
        System.out.println("Making request to: " + TWS_LOCATION);
        System.out.println("Start - Invoking Get Account... " + new Date());        
        EnquiryAccountTWSResponse eatwsr = port.EnquiryAccountTWS(account);
        System.out.println("End - Invoking Get Account... " + new Date());        
        
        JSONObject resp = new JSONObject();
        resp.put("responsecode", eatwsr.getStatus().getSuccessIndicator());
        if (eatwsr.getStatus().getSuccessIndicator() == SuccessIndicator.SUCCESS) {
            IDIEACCTBALTWSType.GIDIEACCTBALTWSDetailType gIDIEACCTBALTWSDetailType = eatwsr.getIDIEACCTBALTWSType().get(0).getGIDIEACCTBALTWSDetailType();
            if (gIDIEACCTBALTWSDetailType == null) {
                resp.put("empty", "0");
            }
            else {
                IDIEACCTBALTWSType.GIDIEACCTBALTWSDetailType.MIDIEACCTBALTWSDetailType mIDIEACCTBALTWSDetailType = gIDIEACCTBALTWSDetailType.getMIDIEACCTBALTWSDetailType().get(4);
                resp.put("cif", mIDIEACCTBALTWSDetailType.getCUSTOMER());
                resp.put("balance", mIDIEACCTBALTWSDetailType.getAVAILBAL());
                resp.put("cocode", mIDIEACCTBALTWSDetailType.getCOCODE());
                resp.put("cocodename", mIDIEACCTBALTWSDetailType.getCOCODENAME());
            }
        }
        else {
            List listMsg = eatwsr.getStatus().getMessages();
            String msg = "";
            for (Object object : listMsg) {
                msg += object + "\n";
            }
            resp.put("responsemsg", msg);
        }
        
        return resp;
    }
    
    public void test() {
        try {
            JSONObject param = new JSONObject();
            param.put("inq_timeout", LibConfig.inquiry_timeout_mubp);
            param.put("username_tws", LibConfig.username_tws);
            param.put("password_tws", LibConfig.password_tws);

            // Get the CIF
            //TWS.configure(LibConfig.loc_tws);

            param.put("account", arrAcc[index]);
            System.out.println(Thread.currentThread().getName() + " - " + this.retrieve(param));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        for (int i=0; i < 1; i++) {
            Class5 class5 = new Class5(i);
            Thread t = new Thread(class5);
            t.start();
            
            try {
                //Thread.sleep(3000);
            }
            catch (Exception e) {
                
            }
        }
    }
    
    public Class5(int index) {
        this.index = index;
    }
    
    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        test();
    }
    
}
