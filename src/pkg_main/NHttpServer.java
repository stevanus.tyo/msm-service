package pkg_main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import org.apache.http.*;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.NHttpConnection;
import org.apache.http.nio.entity.NFileEntity;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.nio.protocol.*;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.http.util.EntityUtils;
import pkg_lib.LibConfig;
import pkg_lib.LibCore;
import pkg_lib.LibFunction;
import pkg_lib.OriginVerification;
import pkg_lib.Setting;
import pkg_lib.UrlEncodedQueryString;

public class NHttpServer {

    static class HttpFileHandler implements HttpAsyncRequestHandler<HttpRequest> {

        private final File docRoot;

        public HttpFileHandler(File docRoot) {
            super();
            this.docRoot = docRoot;
        }

        public HttpAsyncRequestConsumer<HttpRequest> processRequest(
                final HttpRequest request,
                final HttpContext context) {
            return new BasicAsyncRequestConsumer();
        }

        public void handle(
                final HttpRequest request,
                final HttpAsyncExchange httpexchange,
                final HttpContext context) throws HttpException, IOException {
            HttpResponse response = httpexchange.getResponse();
            handleInternal(request, response, context);
            
            httpexchange.submitResponse(new BasicAsyncResponseProducer(response));
        }

        private void handleInternal(
                final HttpRequest request,
                final HttpResponse response,
                final HttpContext context) throws HttpException, IOException {

            HttpCoreContext coreContext = HttpCoreContext.adapt(context);

            String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
            if (!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST")) {
                throw new MethodNotSupportedException(method + " method not supported");
            } else {
                try {
                    String target = request.getRequestLine().getUri();

                    String[] arr_url = LibFunction.SplitString(target, "?");

                    File file = null;
                    String content = null;
                    NHttpConnection conn = coreContext.getConnection(NHttpConnection.class);

                    HttpInetConnection connection = (HttpInetConnection) context.getAttribute(ExecutionContext.HTTP_CONNECTION);
                    InetAddress InetAdd = connection.getRemoteAddress();

                    response.setStatusCode(HttpStatus.SC_OK);
                    //System.out.println(request.getRequestLine());
                    //LibFunction.setLogMessage(request.getRequestLine().toString());
                    //LibFunction.setLogMessage(Arrays.asList(request.getAllHeaders()).toString());
                    HashMap<String,String> hmHeader = new HashMap<String,String>();
                    for (Header header : request.getAllHeaders()) {
                        hmHeader.put(header.getName(), header.getValue());
                    }
                    if (!hmHeader.containsKey("check"))
                        LibFunction.setLogMessage(Arrays.asList(request.getAllHeaders()).toString());
                    
                    if ("1".equals(Setting.getProperty("enable.csrf.sec", true)) 
                        && hmHeader.containsKey("X-Key"))
                        try {
                            LibFunction.setLogMessage("Verifying request origin: " + InetAdd.getHostAddress() + "...");
                            OriginVerification ov = new OriginVerification(hmHeader.get("X-Key"), LibConfig.origin_ClientSecret
                                                                           , hmHeader.get("X-TIMESTAMP"), hmHeader.get("X-SIGNATURE"), LibConfig.origin_ClientId);
                            ov.execute();
                            LibFunction.setLogMessage("Request origin valid: " + InetAdd.getHostAddress());
                        }
                        catch (Exception e) {
                            LibFunction.setLogMessage("Invalid request origin: " + InetAdd.getHostAddress() + ": " + e.getMessage());

                            return;
                        }

                    HttpEntity entity = null;
                    if (request instanceof HttpEntityEnclosingRequest) {
                        entity = ((HttpEntityEnclosingRequest) request).getEntity();
                    }
                    byte[] post_data;
                    if (entity == null) {
                        post_data = new byte[0];
                    } else {
                        post_data = EntityUtils.toByteArray(entity);
                    }

                    URI uri = new URI(target);
                    UrlEncodedQueryString queryString = UrlEncodedQueryString.parse(uri);
                    System.out.println("Query string: " + queryString);
                    String vn = queryString.get("vn");
                    if (vn == null)
                        vn = LibConfig.menu_prefix;
                    else
                        vn = LibConfig.menu_prefix + "-" + vn;
                    //String vn = null;
                    //String vn = "5.4.0";
                    //String vn = "5.5.0";
                    //String vn = "5.6.0";
                    //String vn = "dev-5.6.0";
//                    String vn = "my-5.6.0";
                    System.out.println("Version name: " + vn);
                    
                    if (arr_url[0].replace("/", "").equals("check")) {
                        NStringEntity body = new NStringEntity("OK");
                        response.setEntity(body);
                    }
                    else
                    if (arr_url[0].replace("/", "").equals("version.html")) {
                      //file = new File(this.docRoot, URLDecoder.decode("/version.html", "UTF-8"));
                      if (vn == null)
                          //file = new File(this.docRoot, URLDecoder.decode("/version.html", "UTF-8"));
                          content = Setting.getProperty("version", true);
                      else
                          //file = new File(this.docRoot, URLDecoder.decode(vn + "/version.html", "UTF-8"));
                          content = Setting.getProperty(vn + "-version", true);
                      //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                      NStringEntity body = new NStringEntity(content);
                      response.setEntity(body);
                    //IOS new Menu HANDLER, 2017-10-08
                    } 
                    /*else if (arr_url[0].replace("/", "").equals("version_br.html")) {
                      file = new File(this.docRoot, URLDecoder.decode("/version_br.html", "UTF-8"));
                      NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                      response.setEntity(body);  
                    }*/ 
                    else if (arr_url[0].replace("/", "").equals("version_ios.html")) {
                      //file = new File(this.docRoot, URLDecoder.decode("/version_ios.html", "UTF-8"));
                      if (vn == null)
                          //file = new File(this.docRoot, URLDecoder.decode("/version_ios.html", "UTF-8"));
                          content = Setting.getProperty("version_ios", true);
                      else
                          //file = new File(this.docRoot, URLDecoder.decode(vn + "/version_ios.html", "UTF-8"));
                          content = Setting.getProperty(vn + "-version_ios", true);
                      //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                      NStringEntity body = new NStringEntity(content);
                      response.setEntity(body);  
                    } 
                    /*else if (arr_url[0].replace("/", "").equals("version_ios_br.html")) {
                      file = new File(this.docRoot, URLDecoder.decode("/version_ios_br.html", "UTF-8"));
                      NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                      response.setEntity(body);  
                    }*/ 
                    // Main Menu
                    /*else if (arr_url[0].replace("/", "").equals("main_menu_ios_br.html")) {
                        if (queryString.get("language") != null) {
                            if (queryString.get("language").equals("en")) {
                                file = new File(this.docRoot, URLDecoder.decode("/menu_ios_en_br.html", "UTF-8"));
                            } else {
                                file = new File(this.docRoot, URLDecoder.decode("/menu_ios_id_br.html", "UTF-8"));
                            }
                        } else {
                            //System.out.println("Request: menu_ios_id.html");
                            file = new File(this.docRoot, URLDecoder.decode("/menu_ios_id_br.html", "UTF-8"));
                        }
                        NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        response.setEntity(body);
                    //END IOS new Menu HANDLER, 2017-10-08    

                    }*/ 
                    else if (arr_url[0].replace("/", "").equals("main_menu_ios.html")) {
                        if (queryString.get("language") != null) {
                            if (queryString.get("language").equals("en")) {
                                //file = new File(this.docRoot, URLDecoder.decode("/menu_ios_en.html", "UTF-8"));
                                if (vn == null)
                                    //file = new File(this.docRoot, URLDecoder.decode("/menu_ios_en.html", "UTF-8"));
                                    content = Setting.getProperty("menu_ios_en", true);
                                else
                                    //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_ios_en.html", "UTF-8"));
                                    content = Setting.getProperty(vn + "-menu_ios_en", true);
                                    
                            } else {
                                //file = new File(this.docRoot, URLDecoder.decode("/menu_ios_id.html", "UTF-8"));
                                if (vn == null)
                                    //file = new File(this.docRoot, URLDecoder.decode("/menu_ios_id.html", "UTF-8"));
                                    content = Setting.getProperty("menu_ios_id", true);
                                else
                                    //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_ios_id.html", "UTF-8"));
                                    content = Setting.getProperty(vn + "-menu_ios_id", true);
                            }
                        } else {
                            //System.out.println("Request: menu_ios_id.html");
                            //file = new File(this.docRoot, URLDecoder.decode("/menu_ios_id.html", "UTF-8"));
                            if (vn == null)
                                //file = new File(this.docRoot, URLDecoder.decode("/menu_ios_id.html", "UTF-8"));
                                content = Setting.getProperty("menu_ios_id", true);
                            else
                                //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_ios_id.html", "UTF-8"));
                                content = Setting.getProperty(vn + "-menu_ios_id", true);
                        }
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    //END IOS new Menu HANDLER, 2017-10-08    
                        
                    }
                    /*else if (arr_url[0].replace("/", "").equals("main_menu_br.html")) {
                        if (queryString.get("language") != null) {
                            if (queryString.get("language").equals("en")) {
                                file = new File(this.docRoot, URLDecoder.decode("/menu_en_br.html", "UTF-8"));
                            } else {
                                file = new File(this.docRoot, URLDecoder.decode("/menu_id_br.html", "UTF-8"));
                            }
                        } else {
                            //System.out.println("Request: menu_id.html");
                            file = new File(this.docRoot, URLDecoder.decode("/menu_id_br.html", "UTF-8"));
                        }
                        NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        response.setEntity(body);    
                    }*/ 
                    else if (arr_url[0].replace("/", "").equals("main_menu.html")) {
                        if (queryString.get("language") != null) {
                            if (queryString.get("language").equals("en")) {
                                //file = new File(this.docRoot, URLDecoder.decode("/menu_en.html", "UTF-8"));
                                if (vn == null)
                                    //file = new File(this.docRoot, URLDecoder.decode("/menu_en.html", "UTF-8"));
                                    content = Setting.getProperty("menu_en", true);
                                else
                                    //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_en.html", "UTF-8"));
                                    content = Setting.getProperty(vn + "-menu_en", true);
                            } else {
                                //file = new File(this.docRoot, URLDecoder.decode("/menu_id.html", "UTF-8"));
                                if (vn == null)
                                    //file = new File(this.docRoot, URLDecoder.decode("/menu_id.html", "UTF-8"));
                                    content = Setting.getProperty("menu_id", true);
                                else
                                    //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_id.html", "UTF-8"));
                                    content = Setting.getProperty(vn + "-menu_id", true);
                            }
                        } else {
                            //System.out.println("Request: menu_id.html");
                            //file = new File(this.docRoot, URLDecoder.decode("/menu_id.html", "UTF-8"));
                            if (vn == null)
                                //file = new File(this.docRoot, URLDecoder.decode("/menu_id.html", "UTF-8"));
                                content = Setting.getProperty("menu_id", true);
                            else
                                //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_id.html", "UTF-8"));
                                content = Setting.getProperty(vn + "-menu_id", true);
                        }
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("menu_index.html")) {
                        //file = new File(this.docRoot, URLDecoder.decode("/menu_index.html", "UTF-8"));
                        if (vn == null)
                            //file = new File(this.docRoot, URLDecoder.decode("/menu_index.html", "UTF-8"));
                            content = Setting.getProperty("menu_index", true);
                        else
                            //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_index.html", "UTF-8"));
                            content = Setting.getProperty(vn + "-menu_index", true);
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("menu_index_ios.html")) {
                        //file = new File(this.docRoot, URLDecoder.decode("/menu_index_ios.html", "UTF-8"));
                        if (vn == null)
                            //file = new File(this.docRoot, URLDecoder.decode("/menu_index_ios.html", "UTF-8"));
                            content = Setting.getProperty("menu_index_ios", true);
                        else
                            //file = new File(this.docRoot, URLDecoder.decode(vn + "/menu_index_ios.html", "UTF-8"));
                            content = Setting.getProperty(vn + "-menu_index_ios", true);
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    /*else if (arr_url[0].replace("/", "").equals("version_update_br.html")) {
                        file = new File(this.docRoot, URLDecoder.decode("/version_update_br.html", "UTF-8"));
                        NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        response.setEntity(body);
                    }*/
                    else if (arr_url[0].replace("/", "").equals("version_update.html")) {
                        //file = new File(this.docRoot, URLDecoder.decode("/version_update.html", "UTF-8"));
                        if (vn == null)
                            //file = new File(this.docRoot, URLDecoder.decode("/version_update.html", "UTF-8"));
                            content = Setting.getProperty("version_update", true);
                        else
                            //file = new File(this.docRoot, URLDecoder.decode(vn + "/version_update.html", "UTF-8"));
                            content = Setting.getProperty(vn + "-version_update", true);
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("version_banner.html")) {
                        //file = new File(this.docRoot, URLDecoder.decode("/version_update.html", "UTF-8"));
                        if (vn == null)
                            //file = new File(this.docRoot, URLDecoder.decode("/version_banner.html", "UTF-8"));
                            content = Setting.getProperty("version_banner", true);
                        else
                            //file = new File(this.docRoot, URLDecoder.decode(vn + "/version_banner.html", "UTF-8"));
                            content = Setting.getProperty(vn + "-version_banner", true);
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("version_banner_ios.html")) {
                        //file = new File(this.docRoot, URLDecoder.decode("/version_update.html", "UTF-8"));
                        if (vn == null)
                            //file = new File(this.docRoot, URLDecoder.decode("/version_banner_ios.html", "UTF-8"));
                            content = Setting.getProperty("version_banner_ios", true);
                        else
                            //file = new File(this.docRoot, URLDecoder.decode(vn + "/version_banner_ios.html", "UTF-8"));
                            content = Setting.getProperty(vn + "-version_banner_ios", true);
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    /*else if (arr_url[0].replace("/", "").equals("version_ios_update_br.html")) {
                        file = new File(this.docRoot, URLDecoder.decode("/version_ios_update_br.html", "UTF-8"));
                        NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        response.setEntity(body);
                    }*/
                    else if (arr_url[0].replace("/", "").equals("version_ios_update.html")) {
                        //file = new File(this.docRoot, URLDecoder.decode("/version_ios_update.html", "UTF-8"));
                        if (vn == null)
                            //file = new File(this.docRoot, URLDecoder.decode("/version_ios_update.html", "UTF-8"));
                            content = Setting.getProperty("version_ios_update", true);
                        else
                            //file = new File(this.docRoot, URLDecoder.decode(vn + "/version_ios_update.html", "UTF-8"));
                            content = Setting.getProperty(vn + "-version_ios_update", true);
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("disclaimer.html")) {
                        if (queryString.get("language") != null) {
                            if (queryString.get("language").equals("en")) {
                                    //file = new File(this.docRoot, URLDecoder.decode("/disclaimer_en.html", "UTF-8"));
                                    content = Setting.getProperty("disclaimer_en", true);
                            } else {
                                    //file = new File(this.docRoot, URLDecoder.decode("/disclaimer_id.html", "UTF-8"));
                                    content = Setting.getProperty("disclaimer_id", true);
                            }
                        } else {
                          //file = new File(this.docRoot, URLDecoder.decode("/disclaimer_id.html", "UTF-8"));
                          content = Setting.getProperty("disclaimer_id", true);
                        }
                        //NFileEntity body = new NFileEntity(file, ContentType.create("text/html"));
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("config.html")) {
                        content = Setting.getConfig();
                        NStringEntity body = new NStringEntity(content);
                        response.setEntity(body);
                    }
                    else if (arr_url[0].replace("/", "").equals("updateLimitTracking")) {
                        LibCore lib_core = new LibCore();
                        lib_core.setIPAddress(InetAdd.getHostAddress());
                        String result = lib_core.updateLimitTracking(new String(post_data));
                        NStringEntity body = new NStringEntity(result,
                                ContentType.APPLICATION_JSON);
                        response.setEntity(body);
                        lib_core.CloseDatabase();
                        lib_core = null;
                    }
                    else if (arr_url[0].replace("/", "").equals("obtainActCode")) {
                        LibCore lib_core = new LibCore();
                        lib_core.setIPAddress(InetAdd.getHostAddress());
                        String result = lib_core.obtainActCode(new String(post_data));
                        NStringEntity body = new NStringEntity(result,
                                ContentType.APPLICATION_JSON);
                        response.setEntity(body);
                        lib_core.CloseDatabase();
                        lib_core = null;
                    }
                    else {
                        LibCore lib_core = new LibCore();
                        lib_core.setIPAddress(InetAdd.getHostAddress());
                        lib_core.GetRequest(new String(post_data));
                        NStringEntity body = new NStringEntity(lib_core.getHttpResponse(),
                                ContentType.APPLICATION_JSON);
                        response.setEntity(body);
                        lib_core.CloseDatabase();
                        lib_core = null;
                    }
                } catch (Exception Ex) {
                    LibFunction.setLogMessage(Ex.getMessage());
                }
            }
        }
    }
}
