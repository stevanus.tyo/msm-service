/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Mukhlis Yani
 */
public class Class6 implements Runnable {
 
    
    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(3);  
        
        // passes the Task objects to the pool to execute (Step 3)
        pool.execute(new Class6());
        System.out.println("Hello");
        pool.execute(new Class6());
        pool.execute(new Class6());
        pool.execute(new Class6());
        pool.execute(new Class6()); 
         
        // pool shutdown ( Step 4)
        pool.shutdown();
    }

    @Override
    public void run() {
        System.out.println("Thread:" + Thread.currentThread().getName());
    }
    
}
