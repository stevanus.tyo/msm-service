/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Mukhlis Yani
 */
public class Class10 {
 
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(9090);
            Socket clientSocket = null;
//            PrintWriter out = null;
//            BufferedReader in = null;
            
            BufferedInputStream in = null;
            BufferedOutputStream out = null;
            do {
                try {
                    System.out.println("Listening on 9090...");
                    clientSocket = serverSocket.accept();
                    System.out.println("Connected...");
                    
//                    out = new PrintWriter(clientSocket.getOutputStream(), true);
//                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedOutputStream(clientSocket.getOutputStream());
                    in = new BufferedInputStream(clientSocket.getInputStream());
                    

                    //String inputLine;
                    StringBuilder sb = new StringBuilder();
                    byte[] buffer = new byte [1024];
                    int length = 0;
                    while (length > -1 && in.available() > 0) {
                        length = in.read(buffer, 0, buffer.length);
                        if (length > -1)
                            sb.append(new String(buffer, 0 , length));
                    }
                    
                    System.out.println("Input: " + sb.toString());
                    
                    String outputLine = "380000000000000000201804111608046022360999900690050001019BPIH Inq Pelunasan            BSM001019006900CMS00101969     0097051262      4510000241                0900174655";
                    System.out.println("Output: " + outputLine);
                    out.write(outputLine.getBytes());
                    out.flush();
                    System.out.println("Output sent...");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                finally {
                    if (out != null)
                        out.close();
                    if (in != null)
                        in.close();
                    if (clientSocket != null) {
                        clientSocket.close();
                        clientSocket = null;
                    }
                }
            }
            while (true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        //String data = "1000000000";
        //System.out.println(String.format("%04d", data.length()) + data);
        //"100000" -> "00061000000";
        new Class10().run();
    }
    
    
}
