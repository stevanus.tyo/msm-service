package pkg_main;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.impl.nio.DefaultHttpServerIODispatch;
import org.apache.http.impl.nio.DefaultNHttpServerConnection;
import org.apache.http.impl.nio.DefaultNHttpServerConnectionFactory;
import org.apache.http.impl.nio.reactor.DefaultListeningIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.NHttpConnectionFactory;
import org.apache.http.nio.NHttpServerConnection;
import org.apache.http.nio.protocol.HttpAsyncService;
import org.apache.http.nio.protocol.UriHttpAsyncRequestHandlerMapper;
import org.apache.http.nio.reactor.IOEventDispatch;
import org.apache.http.nio.reactor.ListeningIOReactor;
import org.apache.http.protocol.*;
import pkg_lib.LibConfig;
import pkg_lib.LibFunction;
import pkg_lib.LibQuery;
import pkg_lib.Setting;
import pkg_main.NHttpServer.HttpFileHandler;

public final class AppMain {

    public AppMain() {
        LibFunction.setLogMessage("Initializing....");
        try {
            Setting.load();
        }
        catch (Exception e) {
            LibFunction.setLogMessage("Error: " + e.getMessage());
            System.exit(1);
        }
        LibFunction.setLogMessage("Initialized....");
        
        LibFunction.setLogMessage("Try connecting to database....");
        LibQuery mssqlq = new LibQuery();
        if (!mssqlq.OpenConnection()) {
            LibFunction.setLogMessage("Error: Database connection = " + mssqlq.getErrMessage() + "\n");
            try {
                mssqlq.getConn().close();
                mssqlq = null;
            } catch (SQLException Ex) {
                LibFunction.setLogMessage("Error: Database connection = " + Ex.getMessage());
            }
        } else {
            LibFunction.setLogMessage("Connected to database.");
            try {
//                File dir = null;
//                dir = new File(LibFunction.GetCurrentDirectory());
//                if (!dir.canExecute() && !dir.canRead() && !dir.canWrite() && !dir.isDirectory()) {
//                    LibFunction.WriteLogFile("Application Stopped, checking directory failed. : "
//                            + LibFunction.GetCurrentDirectory());
//                    System.exit(0);
//                }
//
//                if (!LibFunction.WriteLogFile("Creating log file...")) {
//                    LibFunction.WriteLogFile("Application Stopped, creating log file failed.");
//                    System.exit(0);
//                }

                LibFunction.setLogMessage("BSM Web Server v1.46 ready.");
                File docRoot = new File(LibFunction.GetCurrentDirectory() + "/webroot/");

                HttpProcessor httpproc = HttpProcessorBuilder.create().add(new ResponseDate()).add(new ResponseServer("BSM/1.1")).add(new ResponseContent()).add(new ResponseConnControl()).build();
                UriHttpAsyncRequestHandlerMapper reqistry = new UriHttpAsyncRequestHandlerMapper();
                reqistry.register("/*", new HttpFileHandler(docRoot));
                HttpAsyncService protocolHandler = new HttpAsyncService(httpproc, reqistry) {

                    @Override
                    public void connected(final NHttpServerConnection conn) {
                        System.out.println(conn + ": connection open");
                        super.connected(conn);
                    }

                    @Override
                    public void closed(final NHttpServerConnection conn) {
                        System.out.println(conn + ": connection closed");
                        super.closed(conn);
                    }
                };

                NHttpConnectionFactory<DefaultNHttpServerConnection> connFactory;

                connFactory = new DefaultNHttpServerConnectionFactory(ConnectionConfig.DEFAULT);

                IOEventDispatch ioEventDispatch = new DefaultHttpServerIODispatch(protocolHandler, connFactory);
                IOReactorConfig config = IOReactorConfig.custom()
                        .setIoThreadCount(LibConfig.multi_thread)
                        .setSoTimeout(0)
                        .setConnectTimeout(3000)
                        .build();
                ListeningIOReactor ioReactor = new DefaultListeningIOReactor(config);
                ioReactor.listen(new InetSocketAddress(LibConfig.server_port));
                ioReactor.execute(ioEventDispatch);

                System.out.println("Shutdown");

            } catch (IOException e) {
                LibFunction.setLogMessage("Error: " + e.getMessage());
            }
        }

    }

    
}