/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.binary.Base64;
import org.json.me.JSONObject;
import pkg_lib.LibCNCrypt;
import pkg_lib.LibConfig;
import pkg_lib.LibFunction;

/**
 *
 * @author Mukhlis Yani
 */
public class Class9  implements Runnable {
    
    //private static String url = "http://localhost:9080/";
    //private static String url = "http://10.0.17.25:9080/";
    private static String url = "http://10.0.0.145:9080/";
    private String msg;
    
    public String send(String url, String params) throws Exception {
//        TrustManager[] trustAllCerts = new TrustManager[] { 
//            new X509TrustManager() {     
//                public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
//                    return new X509Certificate[0];
//                } 
//                public void checkClientTrusted( 
//                    java.security.cert.X509Certificate[] certs, String authType) {
//                    } 
//                public void checkServerTrusted( 
//                    java.security.cert.X509Certificate[] certs, String authType) {
//                }
//            } 
//        }; 

        // Install the all-trusting trust manager
        //try {
//            SSLContext sc = SSLContext.getInstance("SSL"); 
//            sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//        } catch (GeneralSecurityException e) {
//            
//        } 
        System.out.println("Sending 'POST' request to URL : " + url);
        URL obj = new URL(url);
        //HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setReadTimeout(60000);

        // optional default is GET
        con.setRequestMethod("POST");
//        con.setHostnameVerifier(new HostnameVerifier()
//        {      
//            public boolean verify(String hostname, SSLSession session)
//            {
//                return true;
//            }
//        });

        //add request header
        //con.setRequestProperty("User-Agent", USER_AGENT);
        //String AUTH_STRING = Base64.encodeBase64URLSafeString("cb4d0291-07d4-47df-b196-0b0291cf316e:6d4d284c-3168-49ad-be62-a0daf22b9cca".getBytes());
//        con.setRequestProperty("Authorization", "Bearer " + jwt);
//        con.setRequestProperty("Accept", "application/json");
//        con.setRequestProperty("Content-Type", "application/json");
//        JSONObject jsParams = new JSONObject(params);
//        if (jsParams.has("requestHeader"))
//            if (jsParams.getJSONObject("requestHeader").has("transactionId"))
//                con.setRequestProperty("transactionId", jsParams.getJSONObject("requestHeader").getString("transactionId"));

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(params);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
           response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println("result:" + response.toString());
        
        return response.toString();
    }
    
    public void updateLimitTracking() {
        try {
            JSONObject params = new JSONObject();
            params.put("msisdn", "085722853409");
            params.put("amount", "-5000");
            params.put("trxtype", 7);

            String url = "http://localhost:9080/updateLimitTracking";
            new Class9().send(url, params.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void obtainActCode() {
        try {
            String pin = "123456";
            String encPIN = LibCNCrypt.encrypt(LibConfig.key_acr, pin);
            System.out.println(encPIN);
            String msg = encPIN;
            //msg = "085643448021_" + (msg);
            //msg = "085643448029_" + (msg);
            //msg = "08197950357_" + (msg);
            //msg = "08192539991_" + (msg);
            //msg = "087889106538_" + (msg);
            //msg = "08562211375_" + (msg);
            //msg = "6281911013328_" + (msg);
            msg = "087882727429_" + (msg);

            System.out.println("Message: " + msg);
            String[] arrStr = msg.split("_");
            for (String string : arrStr) {
                System.out.println("Elmt: " + string);
            }

            //Local host
            //String url = "http://localhost:9080/obtainActCode";
            // 10.1.25.104
            String url = "http://10.1.25.104:9090/obtainActCode";
            //String url = "http://172.20.6.5:9080/obtainActCode";
            new Class9().send(url, msg);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void sendHSMCmd() {
        try {
            String msg = "req_data=%7B%22iccid%22%3A%228962116636946221194f%22%2C%22imei%22%3A%22357325071031470%22%2C%22request_type%22%3A%22payment_inquiry_qrcode%22%2C%22menu_id%22%3A%2200047%22%2C%22device%22%3A%22samsung%22%2C%22device_type%22%3A%22SM-G935F%22%2C%22ip_address%22%3A%22192.168.0.8%22%2C%22language%22%3A%22id%22%2C%22date_local%22%3A%2220180830090133%22%2C%22code%22%3A%22qrpay%22%2C%22customer_id%22%3A%2225643%22%2C%22id_account%22%3A%220097061616%22%2C%22qrcode%22%3A%22OexrsTTgIugsi9RVaHgWpRKWGbRkyVl0g1WdZ5bUowg%3D%22%2C%22pin%22%3A%22B25EB8623865A296%22%7D%23B%2FcLW1cDQ16tdsHYU9C55dJ%2Br71uhG0F%2FPb3y%2F2HC%2FMAC7LJI1aWV1ar1MUdQoV8hOsxYycqm%2FEBmAiaCQUGInd2OXMSQRCTn61lgeItLbtXMmOtLH3dAhGaKRqPeVCmr3b6ZA2xbFQq96BWmS7PbjeA2%2B67mMpOGYn4YnGE9Wg%3D&session_id=20181108111506996899";
            //String msg = "session_id=20180821111238856441&req_data=vRvZ4OTtwVI3PALyus4vX01gO%2FHO%2FKxEN%2B852bTbGD1stzOnqwbFtZT5XP%2BUus3vKWrWMLpsns0298NIHX0gt%2FnLyRC8wWBcZ%2FOvTZhTWAhDS1dkhrMIBCesQTs8%2BWIjLNL2036%2Fnc92pUINMMg8BA%3D%3D%23Ny3d6yvrLOtC%2FnkVAZzqt13YtzUUFCq3x8t4LGMwLxwu0t93lULqi0tDsPNqmdNrULV7kPyMA6u4Sr9EvW9vPoqbVr7YtyNJwNO%2BCDEuRJRwY5GdK0QEC16fjoxLHU69zSMsNHyT4%2F99A3SQuXe3WM%2BXslDgZrLb02FS3keMEs4%3D";
            //String url = "http://172.20.6.5:9080/";
            //String url = "http://localhost:9080/";
            String url = "http://10.0.0.145:9080/";
            send(url, msg);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void sendQRPayPayment() {
        try {
            long threadId = Thread.currentThread().getId();
            
            System.out.println(threadId + "-Start-" + new Date());
            
            msg = "req_data=%7B%22iccid%22%3A%228962116636946221194f%22%2C%22imei%22%3A%22357325071031470%22%2C%22request_type%22%3A%22payment_inquiry_qrcode%22%2C%22menu_id%22%3A%2200047%22%2C%22device%22%3A%22samsung%22%2C%22device_type%22%3A%22SM-G935F%22%2C%22ip_address%22%3A%22192.168.0.8%22%2C%22language%22%3A%22id%22%2C%22date_local%22%3A%2220180830090133%22%2C%22code%22%3A%22qrpay%22%2C%22customer_id%22%3A%22626149%22%2C%22id_account%22%3A%227001499798%22%2C%22qrcode%22%3A%22OexrsTTgIugsi9RVaHgWpRKWGbRkyVl0g1WdZ5bUowg%3D%22%2C%22pin%22%3A%22B25EB8623865A296%22%7D%23B%2FcLW1cDQ16tdsHYU9C55dJ%2Br71uhG0F%2FPb3y%2F2HC%2FMAC7LJI1aWV1ar1MUdQoV8hOsxYycqm%2FEBmAiaCQUGInd2OXMSQRCTn61lgeItLbtXMmOtLH3dAhGaKRqPeVCmr3b6ZA2xbFQq96BWmS7PbjeA2%2B67mMpOGYn4YnGE9Wg%3D&session_id=20181108111506996930&uid=1";
            //msg = "req_data=%7B%22iccid%22%3A%228962116636946221194f%22%2C%22imei%22%3A%22357325071031470%22%2C%22request_type%22%3A%22payment_inquiry_qrcode%22%2C%22menu_id%22%3A%2200047%22%2C%22device%22%3A%22samsung%22%2C%22device_type%22%3A%22SM-G935F%22%2C%22ip_address%22%3A%22192.168.0.8%22%2C%22language%22%3A%22id%22%2C%22date_local%22%3A%2220180830090133%22%2C%22code%22%3A%22qrpay%22%2C%22customer_id%22%3A%2225868%22%2C%22id_account%22%3A%227063370309%22%2C%22qrcode%22%3A%22OexrsTTgIugsi9RVaHgWpRKWGbRkyVl0g1WdZ5bUowg%3D%22%2C%22pin%22%3A%22B25EB8623865A296%22%7D%23B%2FcLW1cDQ16tdsHYU9C55dJ%2Br71uhG0F%2FPb3y%2F2HC%2FMAC7LJI1aWV1ar1MUdQoV8hOsxYycqm%2FEBmAiaCQUGInd2OXMSQRCTn61lgeItLbtXMmOtLH3dAhGaKRqPeVCmr3b6ZA2xbFQq96BWmS7PbjeA2%2B67mMpOGYn4YnGE9Wg%3D&session_id=20181108111506996898";
            //String url = "http://10.0.17.25:9080/";
            JSONObject result = new JSONObject(send(url, msg));
            String trxId = result.getString("transaction_id");
            
            //Thread.sleep(1000);
            
            msg = "req_data=%7B%22iccid%22%3A%228962116636946221194f%22%2C%22imei%22%3A%22357325071031470%22%2C%22request_type%22%3A%22payment_confirm_qrcode%22%2C%22menu_id%22%3A%2200047%22%2C%22device%22%3A%22samsung%22%2C%22device_type%22%3A%22SM-G935F%22%2C%22ip_address%22%3A%22192.168.0.8%22%2C%22language%22%3A%22id%22%2C%22date_local%22%3A%2220180830090151%22%2C%22code%22%3A%22qrpay%22%2C%22customer_id%22%3A%2225643%22%2C%22id_account%22%3A%220097061616%22%2C%22qrcode%22%3A%22OexrsTTgIugsi9RVaHgWpRKWGbRkyVl0g1WdZ5bUowg%3D%22%2C%22pin%22%3A%22B25EB8623865A296%22%2C%22transaction_id%22%3A%22[trxId]%22%2C%22amount%22%3A%2210000%22%2C%22admfee%22%3A%223000%22%2C%22tips%22%3A%220%22%2C%22remark%22%3A%22jajan%22%7D%23AWv%2BNoVMTcJowE%2BwO%2BYAACU9Dq2EN4IjVKHJGEEgt7NhZLmamYkSN7sI%2FmUeOBO%2Ba4PZ8IHIRJuIaE40tRQTAEb0D1pPe73qlv7hjtbP645%2B7e8l%2Fqyma5TCuxjbkTWQf1DD2977iGvWns55CPo%2BAw1Ip82o93Bjeeewphy%2BubY%3D&session_id=20181108111506996899&uid=2";
            msg = msg.replace("[trxId]", trxId);
            send(url, msg);
            
//            Thread.sleep(1000);
//
//            System.out.println("\n");
//            System.out.println(threadId + "-Start-" + new Date());
//            
//            msg = "req_data=%7B%22iccid%22%3A%228962116636946221194f%22%2C%22imei%22%3A%22357325071031470%22%2C%22request_type%22%3A%22payment_qrcode%22%2C%22menu_id%22%3A%2200047%22%2C%22device%22%3A%22samsung%22%2C%22device_type%22%3A%22SM-G935F%22%2C%22ip_address%22%3A%22192.168.0.8%22%2C%22language%22%3A%22id%22%2C%22date_local%22%3A%2220180830090153%22%2C%22code%22%3A%22qrpay%22%2C%22customer_id%22%3A%2225643%22%2C%22id_account%22%3A%220097061616%22%2C%22qrcode%22%3A%22OexrsTTgIugsi9RVaHgWpRKWGbRkyVl0g1WdZ5bUowg%3D%22%2C%22pin%22%3A%22B25EB8623865A296%22%2C%22transaction_id%22%3A%22[trxId]%22%2C%22amount%22%3A%2210000%22%2C%22admfee%22%3A%223000%22%2C%22tips%22%3A%220%22%2C%22remark%22%3A%22jajan%22%7D%23cZk8AhOgOV8UHerV9SZxuSsKZYtHt7y%2FbHkOeHimw51NfA7b6MTlIbfRJyqe8CrRwkK6YXglRBwmjw1h3sD1elEvGeD79NzltNI6XnrXrayu6M%2Bad8MfZKTLz%2FR8fLACiwzcgACQ1lrVOeOeCFaz84aSUrnvMwcKhAzzrDKvXo4%3D&session_id=20181108111506996899";
//            msg = msg.replace("[trxId]", trxId);
//            send(url, msg);
            
            System.out.println(threadId + "-End-" + new Date());
            //System.out.println(result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
//            String strText = "BSM:LAZNASBSMUInfaq;1000;0;0;INFAQ";
//            System.out.println("CN: " + strText);
//            
//            String secretKey = "BSM-QRPay-2018MB";
//            String strEncText = LibCNCrypt.encrypt(secretKey, strText);
//            System.out.println("Encrypted CN: " + strEncText);
//            String strDecText = LibCNCrypt.decrypt(secretKey, strEncText);
//            System.out.println("Decrypted CN: " + strDecText);
            
//            JSONObject menuIndex = new JSONObject();
//            menuIndex.put("btnQRPay", "2");
//            System.out.println(menuIndex.toString());

               //System.out.println(LibFunction.NumberFormat("200000", 0));
//               long t1 = System.currentTimeMillis();
//               Thread.sleep(60000);
//               long t2 = System.currentTimeMillis();
//               System.out.println((t2-t1)/60000 + " min");

                //System.out.println(String.format("%-12s", "123456").replace(" ", "F"));
                //System.out.println("20180709000000".substring(2));
                //System.out.println(String.format(new Locale("ID", "ID"), "%,d", Long.parseLong("150000")));
                //System.out.println(String.format("%.0f", 500.9));
               //System.out.println("1234567890".hashCode() + "");

           //new Class4().obtainActCode();
           
           // Performance Test
           int j = 0;
           int reqCount = 1;
           int count = 1;
           while (j < count) {
                for (int i = 0; i < reqCount; i++) {
                   Class9 class9 = new Class9();
                   Thread t = new Thread(class9);
                   t.start();
                }
                
                System.out.println("Sleep...");
                Thread.sleep(1000);
                j++;
           }
           
//           String[] arrMsg = "ERROR|mdr.biller.provider.ws.genericWS:paymentSoapClientConnector|Read timed out|Timeout".split("\\|");
//           for (int i = 0; i < arrMsg.length; i++)
//               System.out.println(arrMsg[i]);
           
           //System.out.println(String.format("%-16s", "111111").replace(" ", "F"));
           
//           long i = 0;
//           i -= 5;
//           System.out.println(i);

           //System.out.println("0123456789012345".substring(11));
           //System.out.println("23990,23991".contains("2398"));
           
        } 
        catch (Exception ex) {
            Logger.getLogger(LibCNCrypt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        //sendHSMCmd();
        sendQRPayPayment();
    }
    
}
