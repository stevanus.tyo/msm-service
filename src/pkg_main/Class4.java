/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.binary.Base64;
import org.json.me.JSONObject;
import pkg_lib.LibCNCrypt;
import pkg_lib.LibConfig;
import pkg_lib.LibFunction;

/**
 *
 * @author Mukhlis Yani
 */
public class Class4  implements Runnable {
    
    public String send(String url, String params) throws Exception {
//        TrustManager[] trustAllCerts = new TrustManager[] { 
//            new X509TrustManager() {     
//                public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
//                    return new X509Certificate[0];
//                } 
//                public void checkClientTrusted( 
//                    java.security.cert.X509Certificate[] certs, String authType) {
//                    } 
//                public void checkServerTrusted( 
//                    java.security.cert.X509Certificate[] certs, String authType) {
//                }
//            } 
//        }; 

        // Install the all-trusting trust manager
        //try {
//            SSLContext sc = SSLContext.getInstance("SSL"); 
//            sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//        } catch (GeneralSecurityException e) {
//            
//        } 
        System.out.println("\nSending 'POST' request to URL : " + url);
        URL obj = new URL(url);
        //HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setReadTimeout(30000);

        // optional default is GET
        con.setRequestMethod("POST");
//        con.setHostnameVerifier(new HostnameVerifier()
//        {      
//            public boolean verify(String hostname, SSLSession session)
//            {
//                return true;
//            }
//        });

        //add request header
        //con.setRequestProperty("User-Agent", USER_AGENT);
        //String AUTH_STRING = Base64.encodeBase64URLSafeString("cb4d0291-07d4-47df-b196-0b0291cf316e:6d4d284c-3168-49ad-be62-a0daf22b9cca".getBytes());
//        con.setRequestProperty("Authorization", "Bearer " + jwt);
//        con.setRequestProperty("Accept", "application/json");
//        con.setRequestProperty("Content-Type", "application/json");
//        JSONObject jsParams = new JSONObject(params);
//        if (jsParams.has("requestHeader"))
//            if (jsParams.getJSONObject("requestHeader").has("transactionId"))
//                con.setRequestProperty("transactionId", jsParams.getJSONObject("requestHeader").getString("transactionId"));

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(params);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
           response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println("result:" + response.toString());
        
        return response.toString();
    }
    
    public void updateLimitTracking() {
        try {
            JSONObject params = new JSONObject();
            params.put("msisdn", "085722853409");
            params.put("amount", "-5000");
            params.put("trxtype", 7);

            String url = "http://localhost:9080/updateLimitTracking";
            new Class4().send(url, params.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void obtainActCode() {
        try {
            String pin = "123456#id";
            String encPIN = LibCNCrypt.encrypt(LibConfig.key_acr, pin);
            System.out.println(encPIN);
            String msg = encPIN;
            //msg = "085643448021_" + (msg);
            //msg = "085643448029_" + (msg);
            //msg = "08197950357_" + (msg);
            //msg = "08192539991_" + (msg);
            //msg = "087889106538_" + (msg);
            //msg = "08562211375_" + (msg);
            //msg = "6281911013328_" + (msg);
            msg = "087882727429_" + (msg);

            System.out.println("Message: " + msg);
            String[] arrStr = msg.split("_");
            for (String string : arrStr) {
                System.out.println("Elmt: " + string);
            }

            //Local host
            String url = "http://localhost:9080/obtainActCode";
            // 10.1.25.104
            //String url = "http://10.1.25.104:9090/obtainActCode";
            //String url = "http://172.20.6.5:9080/obtainActCode";
            new Class4().send(url, msg);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void sendHSMCmd() {
        try {
            String msg = "session_id=20180824111408268638&req_data=4wz7Xz5kCL89YMrMmlAfcFTVBqAk8g1lRxg3jTV4AvM%3D%23egx7UMcgEaEo4kgs9yX%2FV13hGsDQ2%2Bi0Mb8Rnq7%2FGYBqAeQrCqXXviTAWBBMY5om22rPracUP1Z98fP3J%2BPNJFOJBRVnBoid4SVq%2Fw3Q%2BGGJIWFN%2FTql1tPILKDr36D0k%2Brk%2FsobMLYRsm8Nn5V9CsFziXRpPdMaeTMRThByG9s%3D";
            //String msg = "session_id=20180821111238856441&req_data=vRvZ4OTtwVI3PALyus4vX01gO%2FHO%2FKxEN%2B852bTbGD1stzOnqwbFtZT5XP%2BUus3vKWrWMLpsns0298NIHX0gt%2FnLyRC8wWBcZ%2FOvTZhTWAhDS1dkhrMIBCesQTs8%2BWIjLNL2036%2Fnc92pUINMMg8BA%3D%3D%23Ny3d6yvrLOtC%2FnkVAZzqt13YtzUUFCq3x8t4LGMwLxwu0t93lULqi0tDsPNqmdNrULV7kPyMA6u4Sr9EvW9vPoqbVr7YtyNJwNO%2BCDEuRJRwY5GdK0QEC16fjoxLHU69zSMsNHyT4%2F99A3SQuXe3WM%2BXslDgZrLb02FS3keMEs4%3D";
            //String url = "http://172.20.6.5:9080/";
            //String url = "http://localhost:9080/";
            String url = "http://10.0.0.145:9080/";
            send(url, msg);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
//            String strText = "BSM:LAZNASBSMUInfaq;1000;0;0;INFAQ";
//            System.out.println("CN: " + strText);
//            
//            String secretKey = "BSM-QRPay-2018MB";
//            String strEncText = LibCNCrypt.encrypt(secretKey, strText);
//            System.out.println("Encrypted CN: " + strEncText);
//            String strDecText = LibCNCrypt.decrypt(secretKey, strEncText);
//            System.out.println("Decrypted CN: " + strDecText);
            
//            JSONObject menuIndex = new JSONObject();
//            menuIndex.put("btnQRPay", "2");
//            System.out.println(menuIndex.toString());

               //System.out.println(LibFunction.NumberFormat("200000", 0));
//               long t1 = System.currentTimeMillis();
//               Thread.sleep(60000);
//               long t2 = System.currentTimeMillis();
//               System.out.println((t2-t1)/60000 + " min");

                //System.out.println(String.format("%-12s", "123456").replace(" ", "F"));
                //System.out.println("20180709000000".substring(2));
                //System.out.println(String.format(new Locale("ID", "ID"), "%,d", Long.parseLong("150000")));
                //System.out.println(String.format("%.0f", 500.9));
               //System.out.println("1234567890".hashCode() + "");
               
           new Class4().obtainActCode();
//           int j = 0;
//           int count = 60;
//           while (j < count) {
//                for (int i = 0; i < 50; i++) {
//                   Class4 class4 = new Class4();
//                   Thread t = new Thread(class4);
//                   t.start();
//                }
//                
//                System.out.println("Sleep...");
//                Thread.sleep(1000);
//                j++;
//           }
           
//           String[] arrMsg = "ERROR|mdr.biller.provider.ws.genericWS:paymentSoapClientConnector|Read timed out|Timeout".split("\\|");
//           for (int i = 0; i < arrMsg.length; i++)
//               System.out.println(arrMsg[i]);
           
           //System.out.println(String.format("%-16s", "111111").replace(" ", "F"));
           
//           long i = 0;
//           i -= 5;
//           System.out.println(i);

           //System.out.println("0123456789012345".substring(11));
           //System.out.println("23990,23991".contains("2398"));
           
        } 
        catch (Exception ex) {
            Logger.getLogger(LibCNCrypt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        sendHSMCmd();
    }
    
}
