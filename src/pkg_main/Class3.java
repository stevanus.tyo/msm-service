/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
//import com.lowagie.text.html.simpleparser.HTMLWorker;
//import com.lowagie.text.Document;
//import com.lowagie.text.PageSize;
//import com.lowagie.text.html.simpleparser.HTMLWorker;
//import com.lowagie.text.pdf.PdfWriter;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.json.me.JSONArray;
import org.json.me.JSONObject;
import pkg_lib.EL;
import pkg_lib.LibConfig;
import pkg_lib.LibFunction;

/**
 *
 * @author Mukhlis Yani
 */
public class Class3 {
    
    
    public static void main(String[] args) {
        try {
            String fileName = "wakaf2";
            String file_path = "tmp/" + fileName + ".pdf";
            String imgFile = System.getProperty("user.dir").concat(File.separator).concat("template").concat(File.separator).concat("logo.jpg");
            System.out.println(imgFile);
            
            //String template = LibFunction.ReadFile("template/mubp_payment.html");
            String template = LibFunction.ReadFile("template/" + fileName + ".html");
            
            String pdfTemplate = new String(template.getBytes());
            pdfTemplate = pdfTemplate.replace("{image}", imgFile);
            //System.out.println(template);
            Document document;
          //if (landscape == true) {
            document = new Document(PageSize.LETTER.rotate());
          /*} else {
            document = new Document();
          }*/

          OutputStream pdf_file = new FileOutputStream(new File(file_path));
          PdfWriter writer = PdfWriter.getInstance(document, pdf_file);
          writer.setFullCompression();
          document.open();
          //HTMLWorker htmlWorker = new HTMLWorker(document);
          //htmlWorker.parse(new StringReader(template));
          InputStream is = new ByteArrayInputStream(pdfTemplate.getBytes());
          XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
          document.close();

          pdf_file.close();
          
//            Properties props = new Properties();
//            props.put(LibConfig.smtp_host, "BSM MAIL Server");
//
//            props.put("mail.smtp.port", LibConfig.smtp_port);
//            props.put("mail.smtp.host", LibConfig.smtp_host);
//            //props.put("mail.smtp.auth", "false");
//            props.put("mail.smtp.auth", "true");
//            props.put("mail.smtp.starttls.enable", true);
//
//            Session session = Session.getInstance(props, null);
//            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
//			protected PasswordAuthentication getPasswordAuthentication() {
//				return new PasswordAuthentication("", "");
//			}
//		  });
//            session.setDebug(false);
//
//            MimeMessage msg = new MimeMessage(session);
//
//            msg.setFrom(LibConfig.mail_from);
//            msg.setRecipients(Message.RecipientType.TO, "mukhlisyani@gmail.com");
//            msg.setSubject("");
//            msg.setSentDate(new Date());

//            OutputStream pdf_file = new FileOutputStream(new File(file_path));
//
//            com.lowagie.text.Document document;
//            if (landscape == true) {
//                document = new com.lowagie.text.Document(com.lowagie.text.PageSize.LETTER.rotate());
//            } else {
//                document = new com.lowagie.text.Document();
//            }

//            com.lowagie.text.pdf.PdfWriter.getInstance(document, pdf_file);
//            document.open();
//            HTMLWorker htmlWorker = new HTMLWorker(document);
//            htmlWorker.parse(new StringReader(pdf_content));
//            document.close();
//
//            pdf_file.close();
            
//            String mailTemplate = new String(template.getBytes());;
//            mailTemplate = mailTemplate.replace("{img}", "cid:image");
//
//            MimeMultipart multipart = new MimeMultipart("alternative");
//            MimeBodyPart messageBodyPart = new MimeBodyPart();
//            messageBodyPart.setContent(mailTemplate, "text/html");
//            multipart.addBodyPart(messageBodyPart);
//
//            String fileName = "my.pdf";
//            messageBodyPart = new MimeBodyPart();
//            DataSource source = new FileDataSource(file_path);
//            messageBodyPart.setDataHandler(new DataHandler(source));
//            messageBodyPart.setFileName(fileName);
//            multipart.addBodyPart(messageBodyPart);
//            
//            messageBodyPart = new MimeBodyPart();
//            source = new FileDataSource(imgFile);
//            messageBodyPart.setDataHandler(new DataHandler(source));
//            messageBodyPart.setHeader("Content-ID", "<image>");
//            multipart.addBodyPart(messageBodyPart);
//            
//            msg.setContent(multipart);
//            msg.saveChanges();
//
//            System.out.println(new Date());
//            Transport.send(msg, LibConfig.mail_user, LibConfig.mail_pass);
//            //Transport.send(msg, "", "");
////            Transport t = session.getTransport();
////            t.connect();
////            t.sendMessage(msg, msg.getAllRecipients());
//            System.out.println(new Date());

            //System.out.println("2018091917510240".substring(4));
//            JSONObject x = new JSONObject();
//            x.put("1", "bca");
//            x.put("2", "bsm");
//            x.put("4", "bsm");
//            x.put("3", "mandiri");

//            ArrayList al = new ArrayList();
//            al.add(0, null);
//            al.add(0, null);
//            for (int i = 0; i < al.size(); i++ ) {
//                System.out.println(al.get(i));
//            }
//            System.out.println(al.toString());
            //System.out.println(al.get(10));
            
              
//            Enumeration e = x.keys();
//            while (e.hasMoreElements())
//                System.out.println(e.nextElement());


            
        } catch (Exception ex) {
            Logger.getLogger(Class3.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
