/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mukhlis Yani
 */
public class Class7 implements Runnable {
    private Socket reqSocket = null;
    private OutputStream out = null;
    private InputStream in = null;
    private DataOutputStream dataout = null;
    private DataInputStream datain = null;

    public static void main(String[] args) {
//        for (int i = 0; i < 100 ; i++) {
//            Class7 class7 = new Class7();
//            Thread t = new Thread(class7);
//            t.start();
//        }
//        
        System.out.println(Integer.toHexString(69));
        byte[] str = "  ".getBytes();
        str[0] = 0;
        str[1] = 69;
        System.out.println(new String(str));
    }

    public boolean close() {
        try {
          //out.close();
          dataout.close();
          //in.close();
          datain.close();
          reqSocket.close();
        } catch (IOException e) {
          e.printStackTrace();
    //    } catch (Exception e) {
        }
        in = null;
        out = null;
        dataout = null;
        datain = null;
        reqSocket = null;

        return true;
    }

    public String SendData1(String data) throws Exception {
        reqSocket = new Socket("10.2.109.85", 9191);
        //reqSocket.setKeepAlive(true);
        reqSocket.setSoTimeout(30000);
        //reqSocket.setKeepAlive(false);
        out = reqSocket.getOutputStream();
        //out.flush();
        in = reqSocket.getInputStream();

      dataout = new DataOutputStream(out);
      datain = new DataInputStream(in);

        byte[] buf;
        int length;
        String resp = "";
        try {   
          length = data.length();
          buf = ("  " + data).getBytes();
          buf[0] = (byte) (length >> 8);
          buf[1] = (byte) (length & 0x00FF);
          dataout.write(buf, 0, buf.length);
          dataout.flush();
        } 
        catch (IOException ex) {
          close();
          return resp;
        }

        //RESPONSE
        String errorInfo = "";
        try {
          buf = new byte[2];
          datain.read(buf, 0, 2);
          length = buf[0] << 8 | buf[1];
          buf = new byte[length];
          length = datain.read(buf, 0, buf.length);
          resp = new String(buf, 0, length, "ISO-8859-1");
        } 
        catch (IOException ex) {
          //this.close();//restart jika hsm mati/disconnect
          ex.printStackTrace();
        }
        finally {
          close();
        }
    
        return resp;
  }
      
    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getId() + "-" + 
                    SendData1("0000000000000000000000000106IA4228E17481C29A47;ZZ0"));
                    //SendData1("0000000000000000000000000000NO00"));
        } catch (Exception ex) {
            Logger.getLogger(Class7.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
