/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

/**
 *
 * @author Mukhlis Yani
 */
public class Class8 {

    private SOAPMessage soapm;
    
    public static void getAccountTWS() {
        try {
            Date startDate = new Date();
            System.out.println("Start: " + startDate);
            
            MessageFactory mf = MessageFactory.newInstance();
            SOAPMessage sm = mf.createMessage();
            SOAPPart soapPart = sm.getSOAPPart();
            SOAPEnvelope se = soapPart.getEnvelope();

            String WS_NS2 = "t24";
            //smTK.setProperty(SOAPMessage.CHARACTER_SET_ENCODING, "UTF-8");
            //smTK.setProperty(SOAPMessage.WRITE_XML_DECLARATION ,"true");

            se.addNamespaceDeclaration(WS_NS2, "T24WebServicesImpl");

            SOAPBody sbTK = se.getBody();
            SOAPElement seTK= sbTK.addChildElement("EnquiryPortfolioDPKTWS" , WS_NS2);

            SOAPElement request = seTK.addChildElement("WebRequestCommon");
            //SOAPElement request2 = seTK.addChildElement("OfsFunction");
            //SOAPElement request3 = seTK.addChildElement("FUNDSTRANSFERIDIACCTTRFCMSType");
            //SOAPElement request3 = request2.addChildElement("enquiryInputCollection");

            SOAPElement userName =  request.addChildElement("userName");
            userName.addTextNode("NUGI02");

            SOAPElement password =  request.addChildElement("password");
            password.addTextNode("123123");

//            SOAPElement company =  request.addChildElement("company");
//            company.addTextNode("");

           request = seTK.addChildElement("IDIECUSTDPKPORTFOLIOTWSType");
           request = request.addChildElement("enquiryInputCollection");
//           eic.setColumnName("CUSTOMER.ID");
//            eic.setCriteriaValue(params.getString("cif"));   
//            eic.setOperand("EQ");
           SOAPElement field = request.addChildElement("columnName");
           field.setTextContent("CUSTOMER.ID");
           field = request.addChildElement("criteriaValue");
           field.setTextContent("76272885");
           field = request.addChildElement("operand");
           field.setTextContent("EQ");
           
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            sm.writeTo(baos);
            System.out.println(baos.toString());
            
            SOAPConnectionFactory soapConnectionFactory2 = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection2 = soapConnectionFactory2.createConnection();
            URL url = new URL("http://10.0.17.13:8180/iBSM_TWS/services");
            URLConnection urlConn = url.openConnection();
            urlConn.setReadTimeout(15000);
            SOAPMessage soapResponse = soapConnection2.call(sm, url);
            
            baos = new ByteArrayOutputStream();
            soapResponse.writeTo(baos);
            System.out.println(baos.toString());
            
            Date endDate = new Date();
            System.out.println("End: " + endDate);
            
            System.out.println("Duration: " + (endDate.getTime() - startDate.getTime()));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        //Class8.getAccountTWS();
        //System.out.println((long) 5000.61);
        //System.out.println(Long.MAX_VALUE);
        //BigDecimal bd = new BigDecimal("5000.61");
        //System.out.println(bd.toBigInteger().toString());
        //9,223,372,036,854,775,807
        
        System.out.println(String.format("%02d", 150));
    }
    
}
