/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_main;

import java.io.ByteArrayOutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Time;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Mukhlis Yani
 */
public class Class1 {

    public static void main(String[] args) {
        //try {
            //        JSONArray arr = new JSONArray();
            //        System.out.println(arr.toString());
            //        String str = "";
            //        if (str.isEmpty())
            //            System.out.println("String is empty");
            //String value = "payment_id= ";
            //String[] arrValue = value.split("=");
            //System.out.println(arrValue.length);
            //System.out.println(arrValue);
            //        String custNameTag = "name,nama,customer,pemegang";
            //        System.out.println(custNameTag.contains("pemegang1"));

            //        JSONObject x = new JSONObject();
            //        try {
            //            x.put("1", "xxx");
            //        } catch (JSONException ex) {
            //            Logger.getLogger(Class1.class.getName()).log(Level.SEVERE, null, ex);
            //        }
            //        System.out.println(x);

            //System.out.println(String.format("%-12s", "00000011").replace(" ", "F"));
            //System.out.println(String.format("%" + 6 + "s", "").replace(" ", "X"));

//                    Date ct = new Date();
//                    ct.getTime();
//                    try {
//                        Thread.sleep(1000*30);
//                        Date ct1 = new Date();
//                        System.out.println(((ct1.getTime() - ct.getTime()) / 60000.0));
//                    } catch (InterruptedException ex) {
//                        Logger.getLogger(Class1.class.getName()).log(Level.SEVERE, null, ex);
//                    }

//            String strDataToEncrypt = new String();
//            String strCipherText = new String();
//            String strDecryptedText = new String();
//
//            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
//            SecureRandom sr = new SecureRandom("08128071298".getBytes());
//            keyGen.init(sr);
//            SecretKey secretKey = keyGen.generateKey();
//
//            Cipher aesCipher = Cipher.getInstance("AES");
//            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
//
//            strDataToEncrypt = "any text input";
//            byte[] byteDataToEncrypt = strDataToEncrypt.getBytes();
//            byte[] byteCipherText = aesCipher.doFinal(byteDataToEncrypt);
//            strCipherText = new BASE64Encoder().encode(byteCipherText);
//            System.out.println("cipher text: " +strCipherText);
//            aesCipher.init(Cipher.DECRYPT_MODE,secretKey,aesCipher.getParameters());
//            byte[] byteDecryptedText = aesCipher.doFinal(new BASE64Decoder().decodeBuffer(strCipherText));
//            strDecryptedText = new String(byteDecryptedText);
//            System.out.println("plain text again: " +strDecryptedText);
//        } catch (Exception ex) {
//            Logger.getLogger(Class1.class.getName()).log(Level.SEVERE, null, ex);
//        }

        
          //genac();   
          //System.out.println(1 + String.format("%10s", "").replace(" ", "\n") + 2);
//          String value = "rp12345";
//          if (value.toLowerCase().contains("Rp".toLowerCase()))
//            System.out.println("It contains");

//         String[] arrX = {"1", "2", "3"};
//         System.out.println(Arrays.asList(arrX));
           
            //new Class1().doFT();
            //System.out.println(String.format("%,.0f", 10000000.2));
            //new Class1().test1();
            System.out.println(Long.MAX_VALUE);
            //9,223,372,036,854,775,807
    }
    
    public void test1() {
        String[] msg = "%7B%22iccid%22%3A%228962116636946221194f%22%2C%22imei%22%3A%22357325071031470%22%2C%22request_type%22%3A%22list_account%22%2C%22menu_id%22%3A%2200008%22%2C%22device%22%3A%22samsung%22%2C%22device_type%22%3A%22SM-G935F%22%2C%22ip_address%22%3A%22192.168.0.8%22%2C%22language%22%3A%22id%22%2C%22date_local%22%3A%2220180829065707%22%2C%22customer_id%22%3A%22706%22%7D".split("#");
        System.out.println(msg.length);
    }
    
    public void doFT() {
        try {
            MessageFactory mfTK = MessageFactory.newInstance();
            SOAPMessage smTK = mfTK.createMessage();
            SOAPPart soapPart = smTK.getSOAPPart();
            SOAPEnvelope eTK = soapPart.getEnvelope();

            smTK.setProperty(SOAPMessage.CHARACTER_SET_ENCODING, "UTF-8");
            smTK.setProperty(SOAPMessage.WRITE_XML_DECLARATION ,"true");
            String WS_NS2 = "t24";
            
            eTK.addNamespaceDeclaration(WS_NS2, "T24WebServicesImpl");

            SOAPBody sbTK = eTK.getBody();
            
            SOAPElement seTK= sbTK.addChildElement("FundsTransfer" , WS_NS2);
 
            SOAPElement request = seTK.addChildElement("WebRequestCommon");
            SOAPElement request2 = seTK.addChildElement("OfsFunction");
            SOAPElement request3 = seTK.addChildElement("FUNDSTRANSFERIDIACCTTRFCMSType");
            //SOAPElement request3 = request2.addChildElement("enquiryInputCollection");


            SOAPElement userName =  request.addChildElement("userName");
            userName.addTextNode("TJONDRO");

            SOAPElement password =  request.addChildElement("password");
            password.addTextNode("321321");

            SOAPElement company =  request.addChildElement("company");
            company.addTextNode("ID0010001");

            SOAPElement messageId =  request2.addChildElement("messageId");
            messageId.addTextNode("UBP01");

            SOAPElement fromAccount =  request3.addChildElement("DebitAccount");
            fromAccount.addTextNode("7080000531");

            // SOAPElement amount =  request3.addChildElement("CreditAmount");
            SOAPElement amount =  request3.addChildElement("DebitAmount");
            amount.addTextNode("10000");

            /*
            SOAPElement MSGID =  request3.addChildElement("MSGID");
            MSGID.addTextNode(msgid.toString());
            */
 		
 	    SOAPElement toAccount =  request3.addChildElement("CreditAccount");
 	    toAccount.addTextNode("7080000655");
 	    
            SOAPElement request4 = request3.addChildElement("gPAYMENTDETAILS");
            SOAPElement request5 = request4.addChildElement("PaymentDetails");
            request5.addTextNode("UBP");

            String[] arrFee = "ATTBTBVBCHG:1000,ATASBMCHG:500".split(",");
 	   //if (chargeAmount.longValue() > 0) {
 	    if (arrFee != null) {
	 	   SOAPElement request6 = request3.addChildElement("KodeBiaya");
		   request6.addTextNode("D");
		   
		   int g = arrFee.length;
		   SOAPElement request7 = request3.addChildElement("gCOMMISSIONTYPE");
		   request7.addAttribute(new QName("g"), g + "");
		   
		   for (int h = 0; h < g; h++) {
			   SOAPElement request8 = request7.addChildElement("mCOMMISSIONTYPE");
			   request8.addAttribute(new QName("m"), (h + 1) + "");
			   SOAPElement request9 = request8.addChildElement("JenisBiaya");
			   //request9.addTextNode(chargeCode);
			   String[] fee = arrFee[h].split(":");
			   //request9.addTextNode(chargeCode);
			   request9.addTextNode(fee[0]);
			   SOAPElement request10 = request8.addChildElement("NominalBiaya");
			   //request10.addTextNode(curcode+chargeAmount.toString());
			   request10.addTextNode("IDR"+fee[1]);
 	       }
 	   }
 	   else {
 		   SOAPElement request6 = request3.addChildElement("KodeBiaya");
		   request6.addTextNode("W");
 	   }
	    
	   SOAPElement request11 = request3.addChildElement("MSGID");
	   request11.addTextNode("UBP-01");

            
            smTK.saveChanges();
            //PRINT SOAP REQUEST MESSAGE
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            smTK.writeTo(out);
            System.out.println(out.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void genac() {
        String key = "0123456789";
        int keyLength = key.length();
        byte[] arrKey = key.getBytes();
        byte[] arrKey1 = new byte[keyLength];
        for (int i = 0; i < keyLength; i++)
            arrKey1[keyLength - (i + 1)] = (byte) (arrKey[i] ^ 0b00110011);
        System.out.println(new String(arrKey1).hashCode() + "");
        //System.out.println("0123456189".hashCode());
    }

}