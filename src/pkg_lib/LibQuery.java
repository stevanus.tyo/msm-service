package pkg_lib;

public class LibQuery extends LibMSSQL {

    private String query = null;

    public LibQuery() {
        this.query = null;
    }

    public boolean FreeUpdateQuery(String query) {

        setQuery(query);

        return OpenUpdateStatement();
    }

    public void QueryFreeSelect(String Select) {

        query = "SELECT " + Select.trim();

        setQuery(query);
    }

}
