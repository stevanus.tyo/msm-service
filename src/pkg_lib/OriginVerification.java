/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import java.util.Formatter;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Mukhlis Yani
 */
public class OriginVerification {
    
    private String id;
    private String secret;
    private String x_TimeStamp;
    private String x_Signature;
    private String id1;
    
    private String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    public String calculateHMAC(String data, String key) throws Exception {
        String HMAC_SHA512 = "HmacSHA512";
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), HMAC_SHA512);
        Mac mac = Mac.getInstance(HMAC_SHA512);
        mac.init(secretKeySpec);
        
        return toHexString(mac.doFinal(data.getBytes()));
    }
    
    public OriginVerification(String id, String secret, String x_TimeStamp, String x_Signature
                              , String id1) {
        this.id = id;
        this.secret = secret;
        this.x_TimeStamp = x_TimeStamp;
        this.x_Signature = x_Signature;
        
        this.id1 = id1;
    }

    public void execute() throws Exception {
        if (!id1.equals(id))
            throw new Exception("The client Id is invalid");
        
        String x_Signature1 = calculateHMAC(id + "|" + x_TimeStamp, secret);
        
        if (!x_Signature1.equals(x_Signature))
            throw new Exception("The signature is invalid");
    }
    
}
