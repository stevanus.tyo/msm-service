/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Mukhlis Yani
 */
public class ISCRR {

    private static Integer lock = 1;
    private static String enRR;
    //private static int count;
    private static int currentIC;
    //private static ArrayList listIC;
    
    public static void main(String[] args) {
        
    }
    
    /*static {
        try {
            enRR = Setting.getProperty("enable.roundrobin", false);
            LibFunction.setLogMessage("Enable Round Robin: " + enRR);
            
            if (!"0".equals(enRR)) {
                count = Integer.parseInt(Setting.getProperty("count.ic.roundrobin", false));
                listIC = new ArrayList();
                for (int i = 0; i < count; i++) {
                    String[] ic = Setting.getProperty("ic" + (i + 1) + ".roundrobin", false).split(":");
                    ArrayList ic1 = new ArrayList();
                    ic1.add(ic[0]);
                    ic1.add(Integer.parseInt(ic[1]));
                    listIC.add(ic1);
                }
                currentIC = 0;
                LibFunction.setLogMessage("Count Round Robin: " + count + "\n" +
                                          "List Round Robin: " + listIC + "\n" +
                                          "Count Round Robin: " + currentIC);
            }
        }
        catch (Exception e) {
            LibFunction.setLogMessage("App Error: " + e.toString() + "\n"
                                      + Arrays.toString(e.getStackTrace()).replace("),", "),\n"));
            EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 107, EL.elCritical,
                                      "App Error: " + e.toString());
        }
    }*/

    /*public static ArrayList determine_bak() {
        ArrayList ic;
        if ("0".equals(enRR)) {
            ic = new ArrayList();
            ic.add(LibConfig.iso_url);
            ic.add(LibConfig.iso_port);
            
            LibFunction.setLogMessage("CurrentIC: " + ic);
        }
        else {
            synchronized(lock) {
                ic = (ArrayList) listIC.get(currentIC);
                LibFunction.setLogMessage("CurrentIC: " + currentIC + " - " + ic);

                if (currentIC < (count - 1))
                    currentIC++;
                else
                    currentIC = 0;
            }
        }
        
        return ic;
    }*/
    
    public static ArrayList determine() {
        enRR = Setting.getProperty("enable.roundrobin", true);
        LibFunction.setLogMessage("Enable Round Robin (0: Disabled; 1: Enabled): " + enRR);
        
        ArrayList ic;
        if ("0".equals(enRR)) {
            ic = new ArrayList();
            ic.add(LibConfig.iso_url);
            ic.add(LibConfig.iso_port);
            
            LibFunction.setLogMessage("CurrentIC: " + ic);
        }
        else {
            String[] icArr = Setting.getProperty("ic.roundrobin", true).split(";");
            int maxIC = icArr.length - 1;
            synchronized(lock) {
                if (currentIC > maxIC)
                    currentIC = 0;
                
                String[] icTemp = icArr[currentIC].split(":");
                ic = new ArrayList();
                ic.add(icTemp[0]);
                ic.add(Integer.parseInt(icTemp[1]));
                LibFunction.setLogMessage("CurrentIC: " + currentIC + " - " + ic);

                if (currentIC < maxIC)
                    currentIC++;
                else
                    currentIC = 0;
            }
        }
        
        return ic;
    }
    
}
