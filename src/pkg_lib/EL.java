package pkg_lib;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class EL {
    // Event Level

    public static final int elInformation = 0;
    public static final int elWarning = 1;
    public static final int elHigh = 2;
    public static final int elCritical = 3;
//	public static final int elBuffer = 4;
//	public static final int elSession = 5;
//	public static final int elCommand = 6;
//	public static final int elNewLine = 7;
//	public static final int elDebugInfo = 8;
//	public static final int elDebugLow = 9;
//	public static final int elDebugHigh = 10;
    // Header Type
//	public static final int htNull = 0;
    public static final int htT2 = 1; // ISO8583 message
//	public static final int htU2 = 2;
//	public static final int htS4 = 3;
    public static final int htLineMode = 4; // HTTP or text
    public static final int htAsyncHSM = 5; // HSM message
//	public static final int htISOSeparate = 6;
//	public static final int htT2_LittleEndian = 7;
    // Message Format
    public static final int mfPlainText = 0x0000;
    public static final int mfXml = 0x0100;
    public static final int mfISO8583_Mobile = 0x0110;
    // Header Message Type
//	final static byte hmtTransaction = 6;
//	final static byte hmtTransactionResp = 7;
//	final static byte hmtAuthorization = 8;
//	final static byte hmtCoreBanking = 9;
//	final static byte hmtProperties = 10;                                                                           // ES 2009-05-07, IPC property handler
//	final static byte hmtEMSCommand = 11;
    final static byte hmtEventLog = 12;
    final static byte hmtEventInfo = 13;
//	final static byte hmtEventDebug = 14;
    final static byte hmtEventError = 15;
    final static byte hmtAuditIn = 16;
    final static byte hmtAuditOut = 17;
    final static byte hmtTimeStamp = 18;
//	final static byte hmtCommanderRequest = 19;
//	final static byte hmtCommanderResponse = 20;
//	final static byte hmtKernelSignOn = 21;
    final static byte hmtEventTransaction = 22;
//	final static byte hmtEventStatusChange = 23;
//	final static byte hmtCardManagement = 24;

    public static synchronized void log(String appName, String entity, int eventID, int level, String sLog) {
        byte[] data = createIPC(appName, entity, hmtEventLog, eventID, level, sLog);
        send(data);
    }

    public static synchronized void audit(String appName, String entity, boolean isOut, int header, int format, String sLog, int iLen) {
        byte[] data = createIPC(appName, entity, isOut ? hmtAuditOut : hmtAuditIn, header, format, sLog);
        send(data);
    }

    public static synchronized void trx(String appName, String entity, String info, boolean isUpdate) {
        byte[] data = createIPC(appName, entity, hmtEventTransaction, 0, (isUpdate ? 2 : 1), info);
        send(data);
    }

    private static byte[] createIPC(String appName, String sender, byte messageType, int hi, int lo, String info) {
        int l1 = sender.length();
        int l2 = 0;
        int l3 = info.length();
        int l4 = appName.length();

        byte[] data = new byte[3 + 2 + l1 + 3 + l2 + 3 + l3 + 8 + 8 + 2 + l4];

        int i = 0;

        data[i + 0] = messageType;
        data[i + 1] = 0; // FailCode=hfcSuccess
        data[i + 2] = 0; // SenderType=hetUnknown

        // Sender, length format in LittleEndian
        i = i + 3;
        data[i + 0] = (byte) (l1 >> 8);
        data[i + 1] = (byte) (l1 >> 0);
        System.arraycopy(sender.getBytes(), 0, data, i + 2, l1);

        // Destination
        i = i + 2 + l1;
        data[i + 0] = 0; // DestinationType=hetUnknown
        data[i + 1] = 0; // no destination, length = 0
        data[i + 2] = 0; // no destination, length = 0

        // StringData, length format in LittleEndian 
        i = i + 3;
        data[i + 0] = (byte) (l3 >> 16);
        data[i + 1] = (byte) (l3 >> 8);
        data[i + 2] = (byte) (l3 >> 0);
        System.arraycopy(info.getBytes(), 0, data, i + 3, l3);

        // LongwordData, length format in BigEndian
        i = i + 3 + l3;
        String s = String.format("%04X", hi & 0xFFFF) + String.format("%04X", lo & 0xFFFF);
        System.arraycopy(s.getBytes(), 0, data, i + 0, 8);

        // DateData
        i = i + 8;
        data[i + 0] = 0;
        data[i + 1] = 0;
        data[i + 2] = 0;
        data[i + 3] = 0;
        data[i + 4] = 0;
        data[i + 5] = 0;
        data[i + 6] = 0;
        data[i + 7] = 0;

        // AppName, length format in LittleEndian
        i = i + 8;
        data[i + 0] = (byte) (l4 >> 8);
        data[i + 1] = (byte) (l4 >> 0);
        System.arraycopy(appName.getBytes(), 0, data, i + 2, l4);
        return data;
    }

    private static void send(byte[] data) {
        DatagramSocket socket = null;
        try {
            //  InetAddress host = InetAddress.getByName(LibConfig.log_host);
            // int port = LibConfig.log_port;

            InetAddress host = InetAddress.getByName("localhost");
            int port = 2743;


            socket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(data, data.length, host, port);
            socket.send(packet);

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }
}
