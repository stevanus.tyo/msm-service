package pkg_lib;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.DatatypeConverter;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

// ============================================================================
// Module      : RSA ENCRYPTOR AND DECRYPTOR
// Version     : 1.0
//
// Author      : Andi Irwan <andi.irwan@yahoo.com> ( chicho )
// Copyright   : Copyright (c) LDS. 2014
//               All rights reserved
//
// Application : BSM
//
// Date+Time of change   By    Description
// --------------------- ----- ------------------------------------------------
// June-2014 WIB Andi Irwan Creation of the program
//
// ============================================================================
public class LibRSA {

    KeyPair key_pair;
    KeyFactory key_fact;
    String instance = "";

    public LibRSA(String instance) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            key_fact = KeyFactory.getInstance("RSA");
            this.instance = instance;
        } catch (NoSuchAlgorithmException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
        }
    }

    public void GenerateKeypair() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(1024);
            this.key_pair = keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
        }
    }

    public String GetPublicKeyPem() {
        Key pubKey = key_pair.getPublic();
        String result = Base64.encodeBase64String(pubKey.getEncoded());
        return result;
    }

    public String GetPrivateKeyPem() {
        Key privKey = key_pair.getPrivate();
        String result = Base64.encodeBase64String(privKey.getEncoded());
        return result;
    }

    public PublicKey recoverPublicKey(String pemPubKey) throws Exception {
        byte[] bytes = Base64.decodeBase64(pemPubKey);
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(bytes));
        return publicKey;
    }

    public PrivateKey recoverPrivateKey(String pemPrivKey) throws Exception {
        byte[] bytes = Base64.decodeBase64(pemPrivKey);
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(bytes));
        return privateKey;
    }

    public String EncryptRSA(PublicKey pubKey, String data) {
        String result = "";
        try {
            Cipher cipher = Cipher.getInstance(instance, "BC");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] encrypted = cipher.doFinal(data.getBytes());
            result = DatatypeConverter.printBase64Binary(encrypted);
        } catch (InvalidKeyException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchAlgorithmException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (BadPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (IllegalBlockSizeException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchProviderException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        }
        return result;
    }

    public String DecryptRSA(PrivateKey privKey, String data) {
        String result = "";
        try {
            Cipher cipher = Cipher.getInstance(instance, "BC");
            cipher.init(Cipher.DECRYPT_MODE, privKey);
            byte[] temp = DatatypeConverter.parseBase64Binary(data);
            byte[] decrypted = cipher.doFinal(temp);
            result = new String(decrypted);

        } catch (InvalidKeyException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchAlgorithmException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (BadPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (IllegalBlockSizeException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchProviderException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        }
       
        result = result.replaceAll("[^\\p{ASCII}]", "");
        result = result.replaceAll("[^\\x0A\\x0D\\x20-\\x7E]", "");
        result = result.replaceAll("[^\\x00-\\x7f]", "");
        
        return result;
    }
}
