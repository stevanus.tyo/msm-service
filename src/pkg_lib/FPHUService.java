/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import com.sun.corba.se.impl.orbutil.ObjectWriter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.me.JSONObject;
import static org.bouncycastle.asn1.cms.CMSObjectIdentifiers.data;

/**
 *
 * @author rezamuzay
 */
public class FPHUService {

    static String main() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private Socket reqSocket = null;
    private BufferedOutputStream out = null;
    private BufferedInputStream in = null;
    
//    public static void main(String[] args) { 
//        
//        new FPHUService().run();
//        
//    }
    public JSONObject doInquiry(String... data) throws Exception {
        System.out.println("Making Connection Socket to " + LibConfig.fphu_host + ":" + LibConfig.fphu_port);
        reqSocket = new Socket(LibConfig.fphu_host, LibConfig.fphu_port);
        reqSocket.setSoTimeout(Integer.parseInt(LibConfig.inquiry_timeout_mubp));
        System.out.println("Connection Socket Success");
        //reqSocket.setKeepAlive(false);
        //reqSocket.setKeepAlive(true);
        
        out = new BufferedOutputStream(reqSocket.getOutputStream());
        in = new BufferedInputStream(reqSocket.getInputStream());
        System.out.println("Send Command to Server");
        
        // Send command to server
        out.write(data[0].getBytes());
        out.flush();
        String inputLine, outputLine = null;
        
        StringBuilder sb = new StringBuilder();
        byte[] buffer = new byte [1024];
        int length = 0;
        do
        { 
            length = in.read(buffer, 0, buffer.length);
            if( length > - 1) {
                sb.append(new String(buffer, 0, length));
            }
        }
        while (length > -1 && in.available() > 0);
        outputLine = sb.toString();
            
        String resp1  = outputLine.substring(0, 17);   //17 - Message Identifier
        String resp2  = outputLine.substring(17, 19);  //2  - Response Code KEMENAG
        String resp3  = outputLine.substring(19, 27);  //8  - Unique Reference Number
        String resp4  = outputLine.substring(27, 55);  //28 - Reserved for system
        String resp5  = outputLine.substring(55, 85);  //30 - Reserved for system
        String resp6  = outputLine.substring(85, 125); //40 - Nama Jamaah
        String resp7  = outputLine.substring(125, 128);//3  - Kode Embarkasi
        String resp8  = outputLine.substring(128, 131);//3  - Kode Mata Uang
        String resp9  = outputLine.substring(131, 144);//13 - Nilai Setoran Awal (incl 2 digit)
        String resp10 = outputLine.substring(144, 157);//13 - Biaya BPIH (incl 2 digit)
        String resp11 = outputLine.substring(157, 167);//10 - Kurs USD
        String resp12 = outputLine.substring(167, 180);//13 - Nominal BPIH dalam Rupiah (incl 2 digit)
        String resp13 = outputLine.substring(180, 193);//13 - Nominal Sisa Pelunasan (incl 2 digit)
        String resp14 = outputLine.substring(193, 194);//1  - Flag Lunas Tunda
        String resp15 = outputLine.substring(194, 198);//4  - Tahun Tunda dalam Masehi
        String resp16 = outputLine.substring(198, 199);//1  - Jenis Haji
        String resp17 = outputLine.substring(199, 203);//4  - Kode PIHK (Travel untuk haji Khusus)
        String resp18 = outputLine.substring(203, 238);//35 - Nama PIHK (Travel untuk haji Khusus)
        
        System.out.println("\n === Hasil Respon Inquiry Hajj & Umrah === \n\n" +
                           resp1  + "<= Message Identifier\n" +
                           resp2  + "<= Response Code KEMENAG\n" +
                           resp3  + "<= Unique Reference Number\n" +
                           resp4  + "<= Reserved for system\n" +
                           resp5  + "<= Reserved for system\n" +
                           resp6  + "<= Nama Jamaah\n" +
                           resp7  + "<= Kode Embarkasi\n" +
                           resp8  + "<= Kode Mata Uang\n" +
                           resp9  + "<= Nilai Setoran Awal\n" +
                           resp10 + "<= Biaya BPIH\n" +
                           resp11 + "<= Kurs USD\n" +
                           resp12 + "<= Nominal BPIH Dalam Rupiah\n" +
                           resp13 + "<= Nominal Sisa Pelunasan\n" +
                           resp14 + "<= Flag Lunas Tunda\n" +
                           resp15 + "<= Tahun Tunda Dalam Masehi\n" +
                           resp16 + "<= Jenis Haji\n" +
                           resp17 + "<= Kode PIHK\n" +
                           resp18 + "<= Nama PIHK\n" +
                           "==============================================\n\n"
        );
       
        JSONObject obj = new JSONObject();
        obj.put("nama", resp6); // Nama Jamaah
        obj.put("nominal", resp13.substring(0,resp13.length() - 2)); // Nominal Sisa Pelunasan
        obj.put("respcode", resp2); // Response Code KEMENAG
        obj.put("embarkasi", resp7);
        obj.put("mata_uang", resp8);
        obj.put("setoran_awal", resp9);
        obj.put("biaya_bpih", resp10);
        obj.put("kurs", resp11);
        obj.put("nominal_bpih", resp12);
        obj.put("nominal_sisa", resp13);
        obj.put("flag", resp14);
        obj.put("tahun_tunda", resp15);
        obj.put("jenis_haji", resp16);
        obj.put("kode_pihk", resp17);
        obj.put("nama_pihk", resp18);
                
        return obj;
    }
    
    public JSONObject doPayment(String... data) throws Exception {
        System.out.println("Making Connection Socket to " + LibConfig.fphu_host + ":" + LibConfig.fphu_port);
        reqSocket = new Socket(LibConfig.fphu_host, LibConfig.fphu_port);
        reqSocket.setSoTimeout(Integer.parseInt(LibConfig.payment_timeout_mubp));
        System.out.println("Connection Socket Success");
        //reqSocket.setKeepAlive(false);
        //reqSocket.setKeepAlive(true);
        
        out = new BufferedOutputStream(reqSocket.getOutputStream());
        in = new BufferedInputStream(reqSocket.getInputStream());
        
        System.out.println("Send Command to Server");
        out.write(data[0].getBytes());
        out.flush();
        
        String inputLine, outputLine = null;
        StringBuilder sb = new StringBuilder();
        byte[] buffer = new byte [1024];
        int length = 0;
        do
        { 
            length = in.read(buffer, 0, buffer.length);
            if (length > -1){
                sb.append(new String(buffer, 0, length));
            }
        }
        while (length > -1 && in.available() > 0);
        outputLine = sb.toString();
            
        String resp1  = outputLine.substring(0, 17);    //17 - Message Identifier
        String resp2  = outputLine.substring(17, 19);   //2  - Response Code KEMENAG
        String resp3  = outputLine.substring(19, 27);   //8  - Unique Reference Number
        String resp4  = outputLine.substring(27, 43);   //16 - Reserved for system
        String resp5  = outputLine.substring(43, 83);   //40 - Nama Jamaah
        String resp6  = outputLine.substring(83, 84);   //1  - Jenis Kelamin
        String resp7  = outputLine.substring(84, 109);  //25 - Nama Orang Tua
        String resp8  = outputLine.substring(109, 134); //25 - Tempat Lahir
        String resp9  = outputLine.substring(134, 142); //8  - Tanggal Lahir
        String resp10 = outputLine.substring(142, 145); //3  - Kode Embarkasi
        String resp11 = outputLine.substring(145, 148); //3  - Kode Mata Uang
        String resp12 = outputLine.substring(148, 161); //13 - Biaya BPIH (incl 2 digit)
        String resp13 = outputLine.substring(161, 174); //13 - Nominal BPIH dalam Rupiah (incl 2 digit)
        String resp14 = outputLine.substring(174, 187); //13 - Nominal Sisa Pelunasan (incl 2 digit)
        String resp15 = outputLine.substring(187, 188); //1  - Flag Lunas Tunda
        String resp16 = outputLine.substring(188, 192); //4  - Tahun Tunda dalam Masehi
        String resp17 = outputLine.substring(192, 196); //4  - Tahun Pelunasan dalam Hijriah
        String resp18 = outputLine.substring(196, 200); //4  - Kode PIHK (Travel untuk haji Khusus)
        String resp19 = outputLine.substring(200, 235); //35 - Nama PIHK (Travel untuk haji Khusus)
        String resp20 = outputLine.substring(235, 260); //25 - Alamat Jamaah
        String resp21 = outputLine.substring(260, 285); //25 - Nama Desa / Kelurahan
        String resp22 = outputLine.substring(285, 310); //25 - Nama Kecamatan
        String resp23 = outputLine.substring(310, 335); //25 - Nama Kabupaten / Kota
        String resp24 = outputLine.substring(335, 360); //25 - Nama Propinsi
        String resp25 = outputLine.substring(360, 365); //5  - Kode Pos
        String resp26 = outputLine.substring(365, 380); //15 - Nama Bank
        String resp27 = outputLine.substring(380, 400); //20 - Nama Cabang
        String resp28 = outputLine.substring(400, 425); //25 - Alamat Cabang Bank
        String resp29 = outputLine.substring(425, 428); //3  - Umur (tahun) pada saat pelunasan
        String resp30 = outputLine.substring(428, 430); //2  - Umur (bulan) pada saat pelunasan
        String resp31 = outputLine.substring(430, 434); //4  - Tahun Pelunasan Masehi
        String resp32 = outputLine.substring(434, 466); //32 - Checksum

        System.out.println("\n === Hasil Respon Payment Hajj & Umrah === \n\n" +
                            resp1  + "<= Message Identifier\n" + 
                            resp2  + "<= Response Code KEMENAG\n" + 
                            resp3  + "<= Unique Reference Number\n" + 
                            resp4  + "<= Reserved for system\n" + 
                            resp5  + "<= Nama Jamaah\n" + 
                            resp6  + "<= Jenis Kelamin\n" + 
                            resp7  + "<= Nama Orang Tua\n" + 
                            resp8  + "<= Tempat Lahir\n" + 
                            resp9  + "<= Tanggal Lahir\n" + 
                            resp10 + "<= Kode Embarkasi\n" + 
                            resp11 + "<= Kode Mata Uang\n" + 
                            resp12 + "<= Biaya BPIH (incl 2 digit)\n" + 
                            resp13 + "<= Nominal BPIH dalam Rupiah (incl 2 digit)\n" + 
                            resp14 + "<= Nominal Sisa Pelunasan (incl 2 digit)\n" + 
                            resp15 + "<= Flag Lunas Tunda\n" + 
                            resp16 + "<= Tahun Tunda dalam Masehi\n" + 
                            resp17 + "<= Tahun Pelunasan dalam Hijriah\n" + 
                            resp18 + "<= Kode PIHK (Travel untuk haji Khusus)\n" + 
                            resp19 + "<= Nama PIHK (Travel untuk haji Khusus)\n" + 
                            resp20 + "<= Alamat Jamaah\n" + 
                            resp21 + "<= Nama Desa / Kelurahan\n" + 
                            resp22 + "<= Nama Kecamatan\n" + 
                            resp23 + "<= Nama Kabupaten / Kota\n" + 
                            resp24 + "<= Nama Propinsi\n" + 
                            resp25 + "<= Kode Pos\n" + 
                            resp26 + "<= Nama Bank\n" + 
                            resp27 + "<= Nama Cabang\n" + 
                            resp28 + "<= Alamat Cabang Bank\n" + 
                            resp29 + "<= Umur (Tahun) Pada Saat Pelunasan\n" + 
                            resp30 + "<= Umur (Bulan) Pada Saat Pelunasan\n" + 
                            resp31 + "<= Tahun Pelunasan Masehi\n" + 
                            resp32 + "<= Checksum\n" + 
                            "==============================================\n\n"
        );

        JSONObject obj = new JSONObject();
        obj.put("ref", resp3); // Nomor Transaksi
        obj.put("nostr", resp4); // Nomor Struk
        obj.put("terminal", resp4); // Terminal
        obj.put("nama", resp5); // Nama
        obj.put("embarkasi", resp10); // Embarkasi
        obj.put("nominal", resp14.substring(0,resp14.length() - 2)); // Nominal
        obj.put("tahunmas", resp31); // Tahun Masehi
        obj.put("tahunhij", resp17); // Tahun Hijriah
        obj.put("respcode", resp2); //KEMENAG
        
        return obj;
    }
    
    public JSONObject doCertificate(String... data) throws Exception {
        try {
                  // Send the certificate
                  //localhost:8080/WakafCert/Certificate?
                  //msisdn=081911013328&trxid=FT1234567890&wakifname=Mukhlis Yani&amount=5000000&nazhir=ACT
                  
//                  String params = "msisdn=" + "msisdn";
//                  params += "&" + "trxid=" + "trxid";
//                  params += "&" + "wakifname=" + "Test Saja";
//                  params += "&" + "amount=" + "1000000";
                  // if (wakafType == 0) {
                  //     //params += "&" + "duration=" + "";
                  // }
                  // else {
                  //     params += "&" + "projectname=" + getWakafProjByID(wpId, "name");
                  //     params += "&" + "projectdesc=" + getWakafProjByID(wpId, "description");
                  //     //params += "&" + "duration=" + getWakafProjByID(wpId, "duration");
                  // }
//                  params += "&" + "iscert=" + "1";
//                  params += "&" + "email=" + "test@bsm.co.id";
                  String params = "msisdn=085722853409&trxid=FT16021PXW65&wakifname=Ferdi Haspi Nur Imanulloh&amount=100000.0000&nazhir=Dompet Dhuafa&nazhiraddress=Jakarta&wakaftype=1&projectname=Rumah Sakit Dhuafa&projectdesc=Pembangunan Rumah Sakit Dhuafa&duration=&iscert=1&email=ferdihaspin@gmail.com&nazhirshortname=DD";
                  System.out.println(" =========== Parameter for Certificate =======\n");
                  System.out.println(params);
                  HttpConn httpConn = new HttpConn("http://10.2.224.24:8080/WakafCert/Certificate", params);
                  // HttpConn httpConn = new HttpConn(LibConfig.fphu_cert, params);
                  //httpConn.send();
                  Thread t = new Thread(httpConn);
                  t.start();
                } catch (Exception e) {
                  LibFunction.setLogMessage("App Error: " + e.getMessage());
                  EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 121, EL.elCritical,
                        "App Error: " + e.getMessage());
                }    
        return null;
    }
//    public JSONObject run() {
//        String data = "3000000";
//        try {
////            System.out.println(Thread.currentThread().getId() + "-" +
//            JSONObject doInquiry = doInquiry(String.format("%04d", data.length()) + data);
//            return doInquiry;
////            
//            
//        
//        } catch (Exception ex) {
//            Logger.getLogger(FPHUService.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
}
