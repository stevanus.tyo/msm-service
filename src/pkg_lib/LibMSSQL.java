package pkg_lib;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.sql.*;

public class LibMSSQL {

    private String url = "";
    private String host = "";
    private int port = 0;
    private String username = "";
    private String password = "";
    private String database = "";
    private Connection conn = null;
    private Connection ubp_db_conn = null;
    private Connection qrpay_db_conn = null;
    private Connection inst_db_conn = null;
    private Connection ksei_db_conn = null;
    private Connection svfe_db_conn = null;
    private String query = "";
    public String[] list_query;
    private int q_result = 0;
    private boolean result = false;
    private PreparedStatement prepstmt;
    private Statement statement;
    private ResultSet resultset = null;
    private String err_message = "";
    private boolean reconnect = true;
    public int total_commit = 0;

    public static ComboPooledDataSource cpdsMobile;
    public static ComboPooledDataSource cpdsQRPay;
    public static ComboPooledDataSource cpdsUBP;
    public static ComboPooledDataSource cpdsInst;
    public static ComboPooledDataSource cpdsSMS;
    public static ComboPooledDataSource cpdsKSEI;
    public static ComboPooledDataSource cpdsSVFE;

    public LibMSSQL() {
        try {
//            try {
//                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
//                Class.forName("com.mysql.jdbc.Driver").newInstance();
//            } catch (InstantiationException Ex) {
//                setErrMessage(Ex.getMessage());
//            } catch (IllegalAccessException Ex) {
//                setErrMessage(Ex.getMessage());
//            }

                        
                if (cpdsMobile == null) {
                    cpdsMobile = new ComboPooledDataSource();
                    cpdsMobile.setDriverClass( "com.microsoft.sqlserver.jdbc.SQLServerDriver" ); 
                    cpdsMobile.setJdbcUrl( "jdbc:sqlserver://" + LibConfig.database_host
                        + ":" + LibConfig.database_port + ";"
                        + "databaseName=" + LibConfig.database_db); 
                    cpdsMobile.setUser(LibConfig.database_user); 
                    cpdsMobile.setPassword(LibConfig.database_pass); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsMobile.setMinPoolSize(LibConfig.min_mobile_conn_pool); 
                    cpdsMobile.setAcquireIncrement(LibConfig.inc_mobile_conn_pool); 
                    cpdsMobile.setMaxPoolSize(LibConfig.max_mobile_conn_pool);
                }
                
                if (cpdsQRPay == null) {
                    cpdsQRPay = new ComboPooledDataSource();
                    cpdsQRPay.setDriverClass( "com.mysql.jdbc.Driver" ); 
                    cpdsQRPay.setJdbcUrl( "jdbc:mysql://" + LibConfig.db_host_qrpay
                        + ":" + LibConfig.db_port_qrpay + "/" + LibConfig.db_name_qrpay ); 
                    cpdsQRPay.setUser(LibConfig.db_user_qrpay); 
                    cpdsQRPay.setPassword(LibConfig.db_pass_qrpay); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsQRPay.setMinPoolSize(LibConfig.min_qrpay_conn_pool); 
                    cpdsQRPay.setAcquireIncrement(LibConfig.inc_qrpay_conn_pool); 
                    cpdsQRPay.setMaxPoolSize(LibConfig.max_qrpay_conn_pool);
                    cpdsQRPay.setIdleConnectionTestPeriod(3600);
                    cpdsQRPay.setPreferredTestQuery("select 1");
                }
                
                if (cpdsInst == null) {
                    cpdsInst = new ComboPooledDataSource();
                    cpdsInst.setDriverClass( "com.microsoft.sqlserver.jdbc.SQLServerDriver" ); 
                    cpdsInst.setJdbcUrl( "jdbc:sqlserver://" + LibConfig.db_host_inst
                        + ":" + LibConfig.db_port_inst + ";"
                        + "databaseName=" + LibConfig.db_name_inst ); 
                    cpdsInst.setUser(LibConfig.db_user_inst); 
                    cpdsInst.setPassword(LibConfig.db_pass_inst); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsInst.setMinPoolSize(LibConfig.min_inst_conn_pool); 
                    cpdsInst.setAcquireIncrement(LibConfig.inc_inst_conn_pool); 
                    cpdsInst.setMaxPoolSize(LibConfig.max_inst_conn_pool);
                }
                
                if (cpdsUBP == null) {
                    cpdsUBP = new ComboPooledDataSource();
                    cpdsUBP.setDriverClass( "com.microsoft.sqlserver.jdbc.SQLServerDriver" ); 
                    cpdsUBP.setJdbcUrl( "jdbc:sqlserver://" + LibConfig.db_host_mubp
                        + ":" + LibConfig.db_port_mubp + ";"
                        + "databaseName=" + LibConfig.db_name_mubp ); 
                    cpdsUBP.setUser(LibConfig.db_user_mubp); 
                    cpdsUBP.setPassword(LibConfig.db_pass_mubp); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsUBP.setMinPoolSize(LibConfig.min_ubp_conn_pool); 
                    cpdsUBP.setAcquireIncrement(LibConfig.inc_ubp_conn_pool); 
                    cpdsUBP.setMaxPoolSize(LibConfig.max_ubp_conn_pool);
                }
                
                if (cpdsSMS == null) {
                    cpdsSMS = new ComboPooledDataSource();
                    cpdsSMS.setDriverClass( "com.microsoft.sqlserver.jdbc.SQLServerDriver" ); 
                    cpdsSMS.setJdbcUrl( "jdbc:sqlserver://" + LibConfig.db_host_sms
                        + ":" + LibConfig.db_port_sms + ";"
                        + "databaseName=" + LibConfig.db_name_sms ); 
                    cpdsSMS.setUser(LibConfig.db_user_sms); 
                    cpdsSMS.setPassword(LibConfig.db_pass_sms); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsSMS.setMinPoolSize(LibConfig.min_sms_conn_pool); 
                    cpdsSMS.setAcquireIncrement(LibConfig.inc_sms_conn_pool); 
                    cpdsSMS.setMaxPoolSize(LibConfig.max_sms_conn_pool);
                }
                
                if (cpdsKSEI == null) {
                    cpdsKSEI = new ComboPooledDataSource();
                    cpdsKSEI.setDriverClass( "com.microsoft.sqlserver.jdbc.SQLServerDriver" ); 
                    cpdsKSEI.setJdbcUrl( "jdbc:sqlserver://" + LibConfig.db_host_ksei
                        + ":" + LibConfig.db_port_ksei + ";"
                        + "databaseName=" + LibConfig.db_name_ksei ); 
                    cpdsKSEI.setUser(LibConfig.db_user_ksei); 
                    cpdsKSEI.setPassword(LibConfig.db_pass_ksei); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsKSEI.setMinPoolSize(LibConfig.min_ksei_conn_pool); 
                    cpdsKSEI.setAcquireIncrement(LibConfig.inc_ksei_conn_pool); 
                    cpdsKSEI.setMaxPoolSize(LibConfig.max_ksei_conn_pool);
                }
                
                if (cpdsSVFE == null) {
                    cpdsSVFE = new ComboPooledDataSource();
                    cpdsSVFE.setDriverClass( "oracle.jdbc.driver.OracleDriver"); 
                    cpdsSVFE.setJdbcUrl( "jdbc:oracle:thin:@" + LibConfig.db_host_svfe
                        + ":" + LibConfig.db_port_svfe + ":"
                        + LibConfig.db_name_svfe ); 
                    cpdsSVFE.setUser(LibConfig.db_user_svfe); 
                    cpdsSVFE.setPassword(LibConfig.db_pass_svfe); 
                    // the settings below are optional -- c3p0 can work with defaults 
                    cpdsSVFE.setMinPoolSize(LibConfig.min_svfe_conn_pool); 
                    cpdsSVFE.setAcquireIncrement(LibConfig.inc_svfe_conn_pool); 
                    cpdsSVFE.setMaxPoolSize(LibConfig.max_svfe_conn_pool);
                }

//        } catch (ClassNotFoundException Ex) {
//            setErrMessage(Ex.getMessage());
        } catch (Exception Ex) {
            setErrMessage(Ex.getMessage());
        }

    }

    public Connection getConn() {
        if (this.conn == null) {
            try {
//                DriverManager.setLoginTimeout(LibConfig.db_login_timeout);
//                this.conn = DriverManager.getConnection("jdbc:sqlserver://" + LibConfig.database_host
//                        + ":" + LibConfig.database_port + ";"
//                        + "databaseName=" + LibConfig.database_db + ";"
//                        + "user=" + LibConfig.database_user + ";"
//                        + "password=" + LibConfig.database_pass);
                // this.conn.setTransactionIsolation(this.conn.TRANSACTION_READ_COMMITTED);
                // this.conn.setAutoCommit(false);
                this.conn = cpdsMobile.getConnection();
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        return this.conn;
    }
    
    public Connection getUBPDBConn() {
        if (this.ubp_db_conn == null) {
            try {
//                DriverManager.setLoginTimeout(LibConfig.db_login_timeout);
//                this.ubp_db_conn = DriverManager.getConnection("jdbc:sqlserver://" + LibConfig.db_host_mubp
//                        + ":" + LibConfig.db_port_mubp + ";"
//                        + "databaseName=" + LibConfig.db_name_mubp + ";"
//                        + "user=" + LibConfig.db_user_mubp + ";"
//                        + "password=" + LibConfig.db_pass_mubp);
                this.ubp_db_conn = cpdsUBP.getConnection();
                // this.conn.setTransactionIsolation(this.conn.TRANSACTION_READ_COMMITTED);
                // this.conn.setAutoCommit(false);
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeUBPDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeUBPDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        return this.ubp_db_conn;
    }
    
    public Connection getQRPayDBConn() {
        if (this.qrpay_db_conn == null) {
            try {
//                DriverManager.setLoginTimeout(LibConfig.db_login_timeout);
//                this.qrpay_db_conn = DriverManager.getConnection("jdbc:mysql://" + LibConfig.db_host_qrpay
//                        + ":" + LibConfig.db_port_qrpay + "/" + LibConfig.db_name_qrpay + "?autoreconnect=true",
//                        LibConfig.db_user_qrpay,
//                        LibConfig.db_pass_qrpay);
                this.qrpay_db_conn = cpdsQRPay.getConnection();
                // this.conn.setTransactionIsolation(this.conn.TRANSACTION_READ_COMMITTED);
                // this.conn.setAutoCommit(false);
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeQRPayDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeQRPayDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        return this.qrpay_db_conn;
    }
    
    public Connection getInstDBConn() {
        if (this.inst_db_conn == null) {
            try {
//                DriverManager.setLoginTimeout(LibConfig.db_login_timeout);
//                this.inst_db_conn = DriverManager.getConnection("jdbc:sqlserver://" + LibConfig.db_host_inst
//                        + ":" + LibConfig.db_port_inst + ";"
//                        + "databaseName=" + LibConfig.db_name_inst + ";"
//                        + "user=" + LibConfig.db_user_inst + ";"
//                        + "password=" + LibConfig.db_pass_inst);
                this.inst_db_conn = cpdsInst.getConnection();
                // this.conn.setTransactionIsolation(this.conn.TRANSACTION_READ_COMMITTED);
                // this.conn.setAutoCommit(false);
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeInstDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeInstDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        return this.inst_db_conn;
    }

    public Connection getKSEIDBConn() {
        if (this.ksei_db_conn == null) {
            try {
//                DriverManager.setLoginTimeout(LibConfig.db_login_timeout);
//                this.ubp_db_conn = DriverManager.getConnection("jdbc:sqlserver://" + LibConfig.db_host_mubp
//                        + ":" + LibConfig.db_port_mubp + ";"
//                        + "databaseName=" + LibConfig.db_name_mubp + ";"
//                        + "user=" + LibConfig.db_user_mubp + ";"
//                        + "password=" + LibConfig.db_pass_mubp);
                this.ksei_db_conn = cpdsKSEI.getConnection();
                // this.conn.setTransactionIsolation(this.conn.TRANSACTION_READ_COMMITTED);
                // this.conn.setAutoCommit(false);
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeKSEIDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeKSEIDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        return this.ksei_db_conn;
    }
    
    public Connection getSVFEDBConn() {
        if (this.svfe_db_conn == null) {
            try {
//                DriverManager.setLoginTimeout(LibConfig.db_login_timeout);
//                this.ubp_db_conn = DriverManager.getConnection("jdbc:sqlserver://" + LibConfig.db_host_mubp
//                        + ":" + LibConfig.db_port_mubp + ";"
//                        + "databaseName=" + LibConfig.db_name_mubp + ";"
//                        + "user=" + LibConfig.db_user_mubp + ";"
//                        + "password=" + LibConfig.db_pass_mubp);
                this.svfe_db_conn = cpdsSVFE.getConnection();
                // this.conn.setTransactionIsolation(this.conn.TRANSACTION_READ_COMMITTED);
                // this.conn.setAutoCommit(false);
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeSVFEDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeSVFEDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        return this.svfe_db_conn;
    }

    private void setConn(Connection conn) {
        this.conn = conn;
    }

    public void closeConn() {
        if (this.conn != null) {
            try {
                this.conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }
    
    public void closeUBPDBConn() {
        if (this.ubp_db_conn != null) {
            try {
                this.ubp_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }
    
    public void closeQRPayDBConn() {
        if (this.qrpay_db_conn != null) {
            try {
                this.qrpay_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }
    
    public void closeInstDBConn() {
        if (this.inst_db_conn != null) {
            try {
                this.inst_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }
    
    public void closeKSEIDBConn() {
        if (this.ksei_db_conn != null) {
            try {
                this.ksei_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }
    
    public void closeSVFEDBConn() {
        if (this.svfe_db_conn != null) {
            try {
                this.svfe_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }

    public boolean OpenConnection() {
        try {
            setQuery(null);
            setQueryResult(0);
            setPreStatement(null);
            closeConn();
            setUrl(getHost() + ":" + getPort() + "/" + getDatabase());
            getConn();
        } catch (Exception Ex) {
            setErrMessage(Ex.getMessage());
            return getResult();
        }
        return getResult();
    }

    public boolean getConnectionIsValid() {
        try {
            if (getConn().isValid(0)) {
                setReconnect(false);
                return true;
            } else {
                setReconnect(true);
                RoolBack();
                closeConn();
                return false;
            }
        } catch (SQLException Ex) {
            setReconnect(true);
            RoolBack();
            setErrMessage(Ex.getMessage());
            closeConn();
            return false;
        } catch (Exception Ex) {
            setReconnect(true);
            RoolBack();
            setErrMessage(Ex.getMessage());
            closeConn();
            return false;
        }
    }

    public void Commit() {
        if (getConn() != null) {
            try {
                getConn().commit();
            } catch (SQLException Ex) {
                RoolBack();
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                setErrMessage(Ex.getMessage());
            }
        }
    }

    public void RoolBack() {
        if (getConn() != null) {
            try {
                getConn().rollback();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }

    public void CloseResultSet() {
        if (getStatement() != null) {
            try {
                this.resultset.close();
            } catch (SQLException Ex) {
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                setErrMessage(Ex.getMessage());
            }
        }
    }

    // Create Statement
    public Statement getStatement() {
        try {
            setStatement((Statement) getConn().createStatement());
        } catch (SQLException Ex) {
            setErrMessage(Ex.getMessage());
        } catch (Exception Ex) {
            setErrMessage(Ex.getMessage());
        }
        return this.statement;
    }

    private void setStatement(Statement statement) {
        this.statement = statement;
    }

    public ResultSet OpenSelectStatement() {
        ResultSet rs = null;
        if (!this.query.isEmpty() || this.query != null) {
            //if (getConnectionIsValid()) {
            try {
                rs = getStatement().executeQuery(this.query);
            } catch (SQLException Ex) {
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                setErrMessage(Ex.getMessage());
            }
            //}
        }
        return rs;
    }

    public boolean OpenUpdateStatement() {
        if (getConnectionIsValid()) {
            try {
                total_commit = 0;
                getStatement().setEscapeProcessing(true);
                this.q_result = getStatement().executeUpdate(this.query);
                total_commit = this.q_result;
                if (total_commit == 0) {
                    return false;
                }
            } catch (SQLException Ex) {
                RoolBack();
                setErrMessage(Ex.getMessage());
                return false;
            } catch (Exception Ex) {
                RoolBack();
                setErrMessage(Ex.getMessage());
                return false;
            }
        }
        return true;
    }

    // Prepared Statement
    public PreparedStatement getPreStatement() {
        try {
            if (this.prepstmt == null || this.prepstmt.isClosed()) {
                this.prepstmt = (PreparedStatement) getConn().prepareStatement(this.query);
            }
        } catch (SQLException Ex) {
            setErrMessage(Ex.getMessage());
        } catch (Exception Ex) {
            setErrMessage(Ex.getMessage());
        }
        return this.prepstmt;
    }

    public void setPreStatement(PreparedStatement prepstmt) {
        this.prepstmt = prepstmt;
    }

    public ResultSet OpenSelecPreStatement() {
        if (!this.query.isEmpty() || this.query != null) {
            if (getConnectionIsValid()) {
                try {
                    getPreStatement().setEscapeProcessing(true);
                    this.resultset = getPreStatement().executeQuery();
                } catch (SQLException Ex) {
                    setErrMessage(Ex.getMessage());
                } catch (Exception Ex) {
                    setErrMessage(Ex.getMessage());
                }
            }
        }
        return resultset;
    }

    public boolean OpenUpdatePreStatement() {
        if (getConnectionIsValid()) {
            try {
                getPreStatement().setEscapeProcessing(true);
                this.q_result = getPreStatement().executeUpdate();
                if (this.q_result == 0) {
                    return false;
                }
            } catch (SQLException Ex) {
                RoolBack();
                setErrMessage(Ex.getMessage());
                return false;
            } catch (Exception Ex) {
                RoolBack();
                setErrMessage(Ex.getMessage());
                return false;
            }
        }
        return true;
    }

    public void ClosePreStatement() {
        if (getPreStatement() != null) {
            try {
                getPreStatement().close();
            } catch (SQLException Ex) {
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                setErrMessage(Ex.getMessage());
            }
        }
    }

    public void setErrMessage(String err_message) {
        try {
            err_message = err_message.trim();
            if (!(err_message == null || err_message.isEmpty())) {
                this.err_message = err_message;
            }
        } catch (Exception Ex) {
            this.err_message = Ex.getMessage();
        }
    }

    public String getErrMessage() {
        return this.err_message;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return this.query;
    }

    private void setResult(boolean result) {
        this.result = result;
    }

    public boolean getResult() {
        return this.result;
    }

    private void setQueryResult(int q_result) {
        this.q_result = q_result;
    }

    public int getQueryResult() {
        return q_result;
    }

    public String getDatabase() {
        return this.database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUrl() {

        return "jdbc:sqlserver://" + LibConfig.db_connection_properties;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setReconnect(boolean reconnect) {
        this.reconnect = reconnect;
    }

    public boolean getReconnect() {
        return this.reconnect;
    }
}
