package pkg_lib;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;
import org.json.me.JSONObject;

public class Setting {

	private static DataSource dataSource;
	private static Properties properties;

	public static void load() throws Exception {
		properties = new Properties();
                
                // Load the properties from the file
		//load("app.properties");
                
		// Load the properties from the database
		//dataSource = ServiceFinder.getDataSource(properties.getProperty("jdbc/jndi/app"));
		loadSettingDB();
	}

	private static void load(String settingFileName) throws Exception {
		InputStream is = null;
		properties = new Properties();
		
		try {
			is = Setting.class.getClassLoader().getResource(Setting.class.getPackage().getName().replace(".", "/") + "/" + settingFileName).openStream();
			properties.load(is);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			throw new Exception("Can not load the setting from the file");
		}
		finally {
			if (is != null)
				try {
					is.close();
				} 
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	private static void loadSettingDB() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// Look up the data source
			conn = new LibMSSQL().getConn();
			ps = conn.prepareStatement("select * from setting", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rs = ps.executeQuery();
			while (rs.next()) {
				//System.out.println(rs.getString("name") + ": " + rs.getString("value"));
				properties.setProperty(rs.getString("name"), rs.getString("value"));
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			
			throw new Exception("Can not load setting from database");
		}
		finally {
			if (rs != null)
				try {
					rs.close();
				} 
				catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (ps != null)
				try {
					ps.close();
				} 
				catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if (conn != null)
				try {
					conn.close();
				} 
				catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public static String getProperty(String key, boolean reload) {
		if (reload) {
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
	
			try {
				conn = new LibMSSQL().getConn();
				ps = conn.prepareStatement("select * from setting where name=?", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
				ps.setObject(1, key);
				rs = ps.executeQuery();
				if (rs.next()) {
					//System.out.println(rs.getString("name") + ": " + rs.getString("value"));
					properties.setProperty(rs.getString("name"), rs.getString("value"));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				
				//throw new Exception("Can not reload setting from database");
			}
			finally {
				if (rs != null)
					try {
						rs.close();
					} 
					catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (ps != null)
					try {
						ps.close();
					} 
					catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if (conn != null)
					try {
						conn.close();
					} 
					catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
		
		return properties.getProperty(key);
	}
        
        public static String getConfig() {
            JSONObject jsReslut = new JSONObject();
            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                conn = new LibMSSQL().getConn();
                ps = conn.prepareStatement("select * from setting where name like 'config.%'", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                rs = ps.executeQuery();
                while (rs.next()) {
                    jsReslut.put(rs.getString("name"), rs.getString("value"));
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                if (rs != null)
                    try {
                        rs.close();
                    } 
                    catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                if (ps != null)
                    try {
                        ps.close();
                    } 
                    catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                if (conn != null)
                    try {
                        conn.close();
                    } 
                    catch (SQLException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
            }
            
            return jsReslut.toString();
	}

	public static void main(String[] args) {
		/*properties = new Properties();

		properties.setProperty("jdbc/jndi", "BRIS_WBI-DS");
		properties.setProperty("bit2", "5022820300000001");
		properties.setProperty("bit14", "6003");
		properties.setProperty("bit35", "5022820300000001=6003121054");
		properties.setProperty("bit52", "233065DF1AF89320");
		properties.setProperty("bit2-6010", "5022820345552537");
		properties.setProperty("bit14-6010", "6003");
		properties.setProperty("bit18-6010", "6010");
		properties.setProperty("bit35-6010", "5022820345552537=60031219250003");
		properties.setProperty("bit52-6010", "DDE870F044FAF981");
		properties.setProperty("bit2-6020", "5022820351191410");
		properties.setProperty("bit14-6020", "6003");
		properties.setProperty("bit18-6020", "6020");
		properties.setProperty("bit35-6020", "5022820351191410=60031210100003");
		properties.setProperty("bit52-6020", "AAB81291B889B6AF");
		
		FileOutputStream fos;
		try {
			fos = new FileOutputStream("wswbi.properties");
			properties.store(fos, null);
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		/*load();
		System.out.println(getProperty("bit52-6020"));*/

		/*String a = "A";
		String b = a;
		a = a.concat("B");
		System.out.println(a);
		System.out.println(b);*/
		
		/*Double a = 10000000.0 / 30;
		Double b = a * 30;
		System.out.println("Value a: " + a);
		System.out.println("Value b: " + b);*/
		
		try {
			Setting.load();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}