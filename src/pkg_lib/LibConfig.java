package pkg_lib;

public class LibConfig {

  public static String db_connection_properties = "autoReconnect=false"
          + "&connectionCollation=latin1_general_ci"
          + "&useGmtMillisForDatetimes=false";
  public static String application_id = "BSM-SERVICE";
  //public static String file_config_xml = LibFunction.GetCurrentDirectory() + "\\appconfig.xml";
  public static String file_config_xml = LibFunction.GetCurrentDirectory() + "/appconfig.xml";
  public static String template_path = LibFunction.GetCurrentDirectory() + "/template/";
  public static String image_path = LibFunction.GetCurrentDirectory() + "/template/logo.jpg";
  public static String log_folder = LibFunction.GetCurrentDirectory() + "/log";
  public static int server_port = Integer.parseInt(XMLParser.XMLConfig("config", "server_port"));
  
  public static int max_mobile_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_mobile_conn_pool"));
  public static int min_mobile_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_mobile_conn_pool"));
  public static int inc_mobile_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_mobile_conn_pool"));
  
  public static int max_ubp_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_ubp_conn_pool"));
  public static int min_ubp_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_ubp_conn_pool"));
  public static int inc_ubp_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_ubp_conn_pool"));
  
  public static int max_qrpay_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_qrpay_conn_pool"));
  public static int min_qrpay_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_qrpay_conn_pool"));
  public static int inc_qrpay_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_qrpay_conn_pool"));
  
  public static int max_inst_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_inst_conn_pool"));
  public static int min_inst_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_inst_conn_pool"));
  public static int inc_inst_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_inst_conn_pool"));
  
  public static int max_sms_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_sms_conn_pool"));
  public static int min_sms_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_sms_conn_pool"));
  public static int inc_sms_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_sms_conn_pool"));
  
  public static int max_ksei_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_ksei_conn_pool"));
  public static int min_ksei_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_ksei_conn_pool"));
  public static int inc_ksei_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_ksei_conn_pool"));
  
  public static int max_svfe_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "max_svfe_conn_pool"));
  public static int min_svfe_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "min_svfe_conn_pool"));
  public static int inc_svfe_conn_pool = Integer.parseInt(XMLParser.XMLConfig("config", "inc_svfe_conn_pool"));
  
  public static String database_host = XMLParser.XMLConfig("config", "db_host");
  public static String database_port = XMLParser.XMLConfig("config", "db_port");
  public static String database_user = XMLParser.XMLConfig("config", "db_username");
  public static String database_pass = XMLParser.XMLConfig("config", "db_password");
  public static String database_db = XMLParser.XMLConfig("config", "db_dbname");
  
  //Mandiri UBP - Start
  public static String db_host_mubp = XMLParser.XMLConfig("config", "db_host_mubp");
  public static String db_port_mubp = XMLParser.XMLConfig("config", "db_port_mubp");
  public static String db_user_mubp = XMLParser.XMLConfig("config", "db_username_mubp");
  public static String db_pass_mubp = XMLParser.XMLConfig("config", "db_password_mubp");
  public static String db_name_mubp = XMLParser.XMLConfig("config", "db_name_mubp");
  
  //KSEI check SRE db - Start
  public static String db_host_ksei = XMLParser.XMLConfig("config", "db_host_ksei");
  public static String db_port_ksei = XMLParser.XMLConfig("config", "db_port_ksei");
  public static String db_user_ksei = XMLParser.XMLConfig("config", "db_username_ksei");
  public static String db_pass_ksei = XMLParser.XMLConfig("config", "db_password_ksei");
  public static String db_name_ksei = XMLParser.XMLConfig("config", "db_name_ksei");
  
//SVFE db - Start
  public static String db_host_svfe = XMLParser.XMLConfig("config", "db_host_svfe");
  public static String db_port_svfe = XMLParser.XMLConfig("config", "db_port_svfe");
  public static String db_user_svfe = XMLParser.XMLConfig("config", "db_username_svfe");
  public static String db_pass_svfe = XMLParser.XMLConfig("config", "db_password_svfe");
  public static String db_name_svfe = XMLParser.XMLConfig("config", "db_name_svfe");
  public static String loc_svfe = XMLParser.XMLConfig("config", "loc_svfe");
  public static String loc_svbo = XMLParser.XMLConfig("config", "loc_svbo");
  //SVFE db - End

  public static String apikey_mubp = XMLParser.XMLConfig("config", "apikey_mubp");
  public static String username_mubp = XMLParser.XMLConfig("config", "username_mubp");
  public static String password_mubp = XMLParser.XMLConfig("config", "password_mubp");
  public static String token_mubp = XMLParser.XMLConfig("config", "token_mubp");
  public static String signature_mubp = XMLParser.XMLConfig("config", "signature_mubp");

  public static String username_tws = XMLParser.XMLConfig("config", "username_tws");
  public static String password_tws = XMLParser.XMLConfig("config", "password_tws");
  public static String username_tws2 = XMLParser.XMLConfig("config", "username_tws2");
  public static String password_tws2 = XMLParser.XMLConfig("config", "password_tws2");
  public static String loc_tws = XMLParser.XMLConfig("config", "loc_tws");
  public static String loc2_tws = XMLParser.XMLConfig("config", "loc2_tws");
  
  public static String inqloc_ws_mubp = XMLParser.XMLConfig("config", "inqloc_ws_mubp");
  public static String pymtloc_ws_mubp = XMLParser.XMLConfig("config", "pymtloc_ws_mubp");
  public static String rvslloc_ws_mubp = XMLParser.XMLConfig("config", "rvslloc_ws_mubp");
  
  public static String inquiry_timeout_mubp = XMLParser.XMLConfig("config", "inquiry_timeout_mubp");
  public static String payment_timeout_mubp = XMLParser.XMLConfig("config", "payment_timeout_mubp");
  public static String reversal_timeout_mubp = XMLParser.XMLConfig("config", "reversal_timeout_mubp");
  
  public static String custNameTerms = XMLParser.XMLConfig("config", "customer_name_term");
  public static String amountTerms = XMLParser.XMLConfig("config", "amount_term");
  //Mandiri UBP - End

  public static String loc_cws = XMLParser.XMLConfig("config", "cws_url");
  public static String loc_cws2 = XMLParser.XMLConfig("config", "cws2_url");
  
  public static String fphu_host = XMLParser.XMLConfig("config", "fphu_host");
  public static String fphu_user = XMLParser.XMLConfig("config", "fphu_user");
  public static String fphu_branch = XMLParser.XMLConfig("config", "fphu_branch");
  public static int fphu_port = Integer.parseInt(XMLParser.XMLConfig("config", "fphu_port"));
  public static String fphu_fee = XMLParser.XMLConfig("config", "fphu_fee");
  
  public static String menu_prefix = XMLParser.XMLConfig("config", "menu_prefix");
    public static String sukuk_host = XMLParser.XMLConfig("config", "sukuk_host");

  public static String key_acr = XMLParser.XMLConfig("config", "key_acr");
  
  public static String host_hsm = XMLParser.XMLConfig("config", "host_hsm");
  public static int port_hsm = Integer.parseInt(XMLParser.XMLConfig("config", "port_hsm"));

  public static String filter_fintrx_acc = XMLParser.XMLConfig("config", "filter_fintrx_acc");
  public static String fintrx_menu_id = XMLParser.XMLConfig("config", "fintrx_menu_id");
  public static String filter_categ_acc = XMLParser.XMLConfig("config", "filter_categ_acc");

  public static int initScope = Integer.parseInt(XMLParser.XMLConfig("config", "init_scope"));
  public static int stepScope = Integer.parseInt(XMLParser.XMLConfig("config", "step_scope"));
  public static int minScope = Integer.parseInt(XMLParser.XMLConfig("config", "min_scope"));
  
  public static String origin_ClientId = XMLParser.XMLConfig("config", "origin_client_id");
  public static String origin_ClientSecret = XMLParser.XMLConfig("config", "origin_client_secret");
  
  //Mandiri EMoney - Start
  public static String clientId = XMLParser.XMLConfig("config", "client_id");
  public static String clientSecret = XMLParser.XMLConfig("config", "client_secret");
  public static String jwtURL = XMLParser.XMLConfig("config", "jwt_url_memoney");
  public static String inqTUURL = XMLParser.XMLConfig("config", "inq_tu_url_memoney");
  public static String pymtTUURL = XMLParser.XMLConfig("config", "pymt_tu_url_memoney");
  public static String inqCUURL = XMLParser.XMLConfig("config", "inq_uc_url_memoney");
  public static String cmdCUURL = XMLParser.XMLConfig("config", "cmd_uc_url_memoney");
  public static String updCUURL = XMLParser.XMLConfig("config", "upd_uc_url_memoney");
  public static String cfmCUURL = XMLParser.XMLConfig("config", "cfm_uc_url_memoney");
  public static String revCUURL = XMLParser.XMLConfig("config", "rev_uc_url_memoney");
  public static String hisCUURL = XMLParser.XMLConfig("config", "his_uc_url_memoney");
  public static String merchantId = XMLParser.XMLConfig("config", "merchant_id");
  public static String terminalId = XMLParser.XMLConfig("config", "terminal_id");
  public static String approvalCode = XMLParser.XMLConfig("config", "approval_code");
  public static String eMoneyFee = XMLParser.XMLConfig("config", "emoney_fee");
  public static String eMoneyIA = XMLParser.XMLConfig("config", "emoney_ia");
  //Mandiri EMoney - End
  
  //Institution
  public static String db_host_inst = XMLParser.XMLConfig("config", "db_host_inst");
  public static String db_port_inst = XMLParser.XMLConfig("config", "db_port_inst");
  public static String db_user_inst = XMLParser.XMLConfig("config", "db_username_inst");
  public static String db_pass_inst = XMLParser.XMLConfig("config", "db_password_inst");
  public static String db_name_inst = XMLParser.XMLConfig("config", "db_name_inst");
  //Institution
  
  //QR Pay - Start
  public static String db_host_qrpay = XMLParser.XMLConfig("config", "db_host_qrpay");
  public static String db_port_qrpay = XMLParser.XMLConfig("config", "db_port_qrpay");
  public static String db_user_qrpay = XMLParser.XMLConfig("config", "db_username_qrpay");
  public static String db_pass_qrpay = XMLParser.XMLConfig("config", "db_password_qrpay");
  public static String db_name_qrpay = XMLParser.XMLConfig("config", "db_name_qrpay");
  public static String key_qrpay = XMLParser.XMLConfig("config", "key_qrpay");
  //QR Pay - End
    
  //SMS Notif - Start
  public static String db_host_sms = XMLParser.XMLConfig("config", "db_host_sms");
  public static String db_port_sms = XMLParser.XMLConfig("config", "db_port_sms");
  public static String db_user_sms = XMLParser.XMLConfig("config", "db_username_sms");
  public static String db_pass_sms = XMLParser.XMLConfig("config", "db_password_sms");
  public static String db_name_sms = XMLParser.XMLConfig("config", "db_name_sms");
  //SMS Notif - End  
  
  //Account Opening - Start
  public static double mabrur_init_dep = Double.parseDouble(XMLParser.XMLConfig("config", "mabrur_init_dep"));
  public static double bsm_min_init_dep = Double.parseDouble(XMLParser.XMLConfig("config", "bsm_min_init_dep"));
  public static double bsm_max_init_dep = Double.parseDouble(XMLParser.XMLConfig("config", "bsm_max_init_dep"));
  public static double bsm_mabrur_init_dep = Double.parseDouble(XMLParser.XMLConfig("config", "bsm_mabrur_init_dep"));
  public static double bsm_wadiah_min_init_dep = Double.parseDouble(XMLParser.XMLConfig("config", "bsm_wadiah_min_init_dep"));
  public static double bsm_wadiah_max_init_dep = Double.parseDouble(XMLParser.XMLConfig("config", "bsm_wadiah_max_init_dep"));
  //Account Opening - End
  
  //Wakaf - Start
  public static double wakaf_init = Double.parseDouble(XMLParser.XMLConfig("config", "wakaf_init"));
  public static double wakaf_cert_amt = Double.parseDouble(XMLParser.XMLConfig("config", "wakaf_cert_amt"));
  public static String wakaf_cert = XMLParser.XMLConfig("config", "wakaf_cert");
  //Wakaf - End
  
  //PCI-DSS - Start
  public static String otpTO = XMLParser.XMLConfig("config", "otp_timeout");
  //PCI-DSS - End
  
  //Is Secured
  public static String act_secured_mb = XMLParser.XMLConfig("config", "act_secured_mb");
  public static String trx_secured_mb = XMLParser.XMLConfig("config", "trx_secured_mb");
  public static String sms_received_to = XMLParser.XMLConfig("config", "sms_received_timeout");
  //Is Secured
  
  //Multi Instance - Start
  public static String instanceId = XMLParser.XMLConfig("config", "instance_id");
  //Multi Instance - Start
  
  public static int ovo_min_amt = Integer.parseInt(XMLParser.XMLConfig("config", "ovo_min_amt"));
  
  public static String iso_url = XMLParser.XMLConfig("config", "iso_url");
  public static int iso_port = Integer.parseInt(XMLParser.XMLConfig("config", "iso_port"));
  //start 28-11-2016
  public static String iso_url_new = XMLParser.XMLConfig("config", "iso_url_new");
  public static int iso_port_new = Integer.parseInt(XMLParser.XMLConfig("config", "iso_port_new"));
  public static String iso_changePin = XMLParser.XMLConfig("iso_to_new", "changePin");
  public static String iso_checkBalance = XMLParser.XMLConfig("iso_to_new", "checkBalance");
  public static String iso_transactionList = XMLParser.XMLConfig("iso_to_new", "transactionList");
  public static String iso_paymentInquiry = "," + XMLParser.XMLConfig("iso_to_new", "paymentInquiry") + ",";
  public static String iso_payment = "," + XMLParser.XMLConfig("iso_to_new", "payment") + ",";
  public static String iso_transferInquiry = "," + XMLParser.XMLConfig("iso_to_new", "transferInquiry") + ",";
  public static String iso_transfer = "," + XMLParser.XMLConfig("iso_to_new", "transfer") + ",";
  public static String iso_inquiryStatus = "," + XMLParser.XMLConfig("iso_to_new", "inquiryStatus") + ",";
  //end 28-11-2016

  public static int db_login_timeout = 30;
  public static int multi_thread = Integer.parseInt(XMLParser.XMLConfig("config", "multi_thread"));
  public static String pln_admin_fee = XMLParser.XMLConfig("config", "pln_admin_fee");
  public static String takaful_fee = XMLParser.XMLConfig("config", "takaful_fee");
  public static String atm_mandiri_fee = XMLParser.XMLConfig("config", "atm_mandiri_fee");
  public static String atm_bersama_fee = XMLParser.XMLConfig("config", "atm_bersama_fee");
  public static String atm_prima_fee = XMLParser.XMLConfig("config", "atm_prima_fee");
  public static String skn_fee = XMLParser.XMLConfig("config", "skn_fee");
  public static String rtgs_fee = XMLParser.XMLConfig("config", "rtgs_fee");
  public static String tunai_fee = XMLParser.XMLConfig("config", "tunai_fee");
  public static String smtp_host = XMLParser.XMLConfig("config", "smtp_host");
  public static int smtp_port = Integer.parseInt(XMLParser.XMLConfig("config", "smtp_port"));
  public static int smtp_conn_timeout = Integer.parseInt(XMLParser.XMLConfig("config", "smtp_conn_timeout"));
  public static int smtp_timeout = Integer.parseInt(XMLParser.XMLConfig("config", "smtp_timeout")); 
  public static String mail_name = XMLParser.XMLConfig("config", "mail_name");
  public static String mail_from = XMLParser.XMLConfig("config", "mail_from");
  public static String mail_user = XMLParser.XMLConfig("config", "mail_user");
  public static String mail_pass = XMLParser.XMLConfig("config", "mail_pass");
  //  public static String log_host = XMLParser.XMLConfig("config", "log_host");
  //  public static int log_port = Integer.parseInt(XMLParser.XMLConfig("config", "log_port"));
  
  public static String mpn_account_perepsi = XMLParser.XMLConfig("config", "mpn_account_perepsi");
  public static String mpn_prefix_bill_djp = XMLParser.XMLConfig("config", "mpn_prefix_bill_djp");
  public static String mpn_prefix_bill_djbc = XMLParser.XMLConfig("config", "mpn_prefix_bill_djbc");
  public static String mpn_prefix_bill_dja = XMLParser.XMLConfig("config", "mpn_prefix_bill_dja");
  public static String mpn_reinquiry_delay = XMLParser.XMLConfig("config", "mpn_reinquiry_delay");
  
  //Financing - Start
  public static String financingUrlInq = XMLParser.XMLConfig("config", "financing_url_inq");
  public static String financingUrlSimulate = XMLParser.XMLConfig("config", "financing_url_simulate");
  public static String financingUrlRequest = XMLParser.XMLConfig("config", "financing_url_request");
  public static String financingDestAccNo = XMLParser.XMLConfig("config", "financing_dest_accno");
  
  public static String split_str = "[split]";
  public static String default_language = "id";
  public static String[] arr_payment_inq = {
    "Payment Reference:",
    "Amount: Rp.",
    "Name:"
  };
  public static String[] arr_transfer_inq = {
    "Source Account Name: ",
    "Beneficiary Account Name: ",
    "Beneficiary Bank Name: "
  };
  public static String[] arr_transfer = {
    "Source Account Name: ",
    "Beneficiary Account Name: ",
    "Transaction ID: "
  };
  public static String[] arr_pln_pre = {
    "Biaya Admin: Rp.",
    "SWReference: ",
    "Segment: ",
    "Category: ",
    "Distribution: ",
    "ServiceUnit: ",
    "ServicePhone: ",
    "MaxKWH: ",
    "Unsold 1: ",
    "Unsold 2: ",
    "Buying Option: ",
    "Receipt Number: ",
    "Stamp Duty: ",
    "PPN: ",
    "PPJ: ",
    "Angsuran: ",
    "Rupiah Token: ",
    "KWH: ",
    "Token: ",
    "TimeStamp: ",
    "STAN: ",
    "PLN Reference: ",
    "Meter ID: ",
    "ID Pelanggan: "
  };
  public static String[] arr_pln_post = {
    "Rp Admin: ",
    "BLTH: ",
    "Alamat: ",
    "Segment: ",
    "Category: ",
    "LWBP: ",
    "WBP: ",
    "KVARH: ",
    "PLN Reference: ",
    "Lembar Tagihan: ",
    "Lembar Tertagih: ",
    "Lembar Tunggak: ",
    "Rp Tunggak: ",
    "Rp Tagihan: ",
    "Rp Denda: ",
    "TrnDate: ",
    "TrnTime: ",
    "Count: ",
    "BLTHs: ",
    "KodeSubsidi: ",
    "Subsidi: ",
    "ServicePhone: ",
    "TimeStamp: ",
    "STAN: "
  };
  public static String[] arr_academic = {
    "Period: ",
    "Item: ",
    "Amount: ",
    "Description: "
  };
  public static String[] arr_insurance = {
    "Installment Value: ",
    "Installment Type: ",
    "Installment Number: ",
    "Due Date: ",
    "Period: "
  };
  public static String[] arr_telkom = {
    "Payment Reference",
    "Payment Amount",
    "Regional: ",
    "District: "
  };
  public static String[] arr_garuda = {
    "Count: ",
    "Status: ",
    "Bank: ",
    "Account Number: ",
    "Account Name: "
  };
  public static String[] arr_garuda_sub = {
    "Flight Info ",
    "Jumlah Penumpang: ",
    "Carrier: ",
    "Kelas: ",
    "From: ",
    "To: ",
    "Flight Number: ",
    "Depature Date: ",
    "Depature Time: "
  };
  public static String[] arr_statement_id = {
    "Tanggal: ",
    "Jam: ",
    "",
    "Keterangan: ",
    "Referensi: "
  };
  public static String[] arr_statement_en = {
    "Date: ",
    "Time: ",
    "",
    "Description: ",
    "Reference: "
  };
  public static String[] arr_inq_wesel = {
    "Nomor Transaksi: ",
    "Nomor Pin: ",
    "Reference Information: "
  };
  public static String[] arr_cash_out = {
    "Nominal Wesel: ",
    "ID Pengirim: ",
    "HP Pengirim: ",
    "Nama Pengirim: ",
    "Alamat Pengirim: ",
    "Alamat Penerima: ",
    "Keterangan: ",
    "Status: "
  };
  public static String[] arr_add_pay_response = {
    "0152",
    "0112",
    "0132"
  };
  public static String[] arr_prepaid = {
    "0111",
    "0121",
    "0131",
    "0133",
    "0141",
    "0151",
    "0181",
    "0211",
    "8001",
    "8002"
  };
  public static String[] arr_postpaid = {
    "0112",
    "0122",
    "0132",
    "0152",
    "0182",
    "0212",
    "0222"
  };
  public static String[] arr_prepaid_payment_amount = {
    "0211"
  };
  public static String[] arr_no_payment_id = {
    "8001",
    "8002"
  };

  //added by suro
  private static int seqStan = 3;

  public static synchronized String getSeq() {
    seqStan++;
    if (("999999").equals(seqStan + "")) {
      seqStan = 4;
    }
    return seqStan + "";
  }

  //start 28-11-2016
  public static void reloadSetting(String forSetting) {
    //iso_url_new = XMLParser.XMLConfig("config", "iso_url_new");
    //iso_port_new = Integer.parseInt(XMLParser.XMLConfig("config", "iso_port_new"));
    if (forSetting.equals("changePin")) {
      iso_changePin = XMLParser.XMLConfig("iso_to_new", "changePin");
    } else if (forSetting.equals("checkBalance")) {
      iso_checkBalance = XMLParser.XMLConfig("iso_to_new", "checkBalance");
    } else if (forSetting.equals("transactionList")) {
      iso_transactionList = XMLParser.XMLConfig("iso_to_new", "transactionList");
    } else if (forSetting.equals("paymentInquiry")) {
      iso_paymentInquiry = "," + XMLParser.XMLConfig("iso_to_new", "paymentInquiry") + ",";
    } else if (forSetting.equals("payment")) {
      iso_payment = "," + XMLParser.XMLConfig("iso_to_new", "payment") + ",";
    } else if (forSetting.equals("transferInquiry")) {
      iso_transferInquiry = "," + XMLParser.XMLConfig("iso_to_new", "transferInquiry") + ",";
    } else if (forSetting.equals("transfer")) {
      iso_transfer = "," + XMLParser.XMLConfig("iso_to_new", "transfer") + ",";
    } else if (forSetting.equals("inquiryStatus")) {
      iso_inquiryStatus = "," + XMLParser.XMLConfig("iso_to_new", "inquiryStatus") + ",";
    }
  }
  
  //add config by prasetyo 
  public static String channelID = XMLParser.XMLConfig("ksei", "channelID");
  public static String inquiryEfek = XMLParser.XMLConfig("ksei", "inquiryEfek");
  public static String inquiryEfekDetail = XMLParser.XMLConfig("ksei", "inquiryEfekDetail");
  public static String balanceInquiry = XMLParser.XMLConfig("ksei", "balanceInquiry");
  public static String balanceInquiryDetail = XMLParser.XMLConfig("ksei", "balanceInquiryDetail");

};
