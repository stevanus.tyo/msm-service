package pkg_lib;

import java.security.*;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.bouncycastle.util.encoders.Base64;

// ============================================================================
// Module      : DES ENCRYPTOR AND DECRYPTOR
// Version     : 1.0
//
// Author      : Andi Irwan <andi.irwan@yahoo.com> ( chicho )
// Copyright   : Copyright (c) LDS. 2014
//               All rights reserved
//
// Application : BSM
//
// Date+Time of change   By    Description
// --------------------- ----- ------------------------------------------------
// June-2014 WIB Andi Irwan Creation of the program
//
// ============================================================================
public class LibDES {

    String instance;

    public LibDES(String instance) {
        Security.addProvider(new com.sun.crypto.provider.SunJCE());
        this.instance = instance;
    }

    public String Encrypt(String key, String data) throws Exception {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(key.getBytes());
            DESKeySpec key_spec = new DESKeySpec(md.digest());
            SecretKeySpec DESKey = new SecretKeySpec(key_spec.getKey(), "DES");
            Cipher cipher = Cipher.getInstance(instance);
            cipher.init(Cipher.ENCRYPT_MODE, DESKey);
            byte[] encrypted = cipher.doFinal(data.getBytes());
            result = Base64.toBase64String(encrypted);
        } catch (InvalidKeyException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchAlgorithmException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (BadPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (IllegalBlockSizeException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        }
        return result;
    }

    public String Decrypt(String key, String data) throws Exception {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(key.getBytes());
            DESKeySpec key_spec = new DESKeySpec(md.digest());
            SecretKeySpec DESKey = new SecretKeySpec(key_spec.getKey(), "DES");
            Cipher cipher = Cipher.getInstance(instance);
            cipher.init(Cipher.DECRYPT_MODE, DESKey);
            byte[] decrypted = cipher.doFinal(Base64.decode(data));
            result = new String(decrypted);
        } catch (InvalidKeyException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchAlgorithmException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (BadPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (IllegalBlockSizeException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        } catch (NoSuchPaddingException Ex) {
            Ex.printStackTrace();
            LibFunction.setLogMessage(Ex.getMessage());
            return "";
        }
        return result;
    }

}
