/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

/**
 *
 * @author Mukhlis Yani
 */
public class HttpConn implements Runnable {

    private static Logger logger = Logger.getLogger(HttpConn.class.getName());
    private String url;
    private String params;
    
    public HttpConn(String url, String params) {
        this.url = url;
        this.params = params;
    }
    
    public String send() throws Exception {
//        TrustManager[] trustAllCerts = new TrustManager[] { 
//            new X509TrustManager() {     
//                public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
//                    return new X509Certificate[0];
//                } 
//                public void checkClientTrusted( 
//                    java.security.cert.X509Certificate[] certs, String authType) {
//                    } 
//                public void checkServerTrusted( 
//                    java.security.cert.X509Certificate[] certs, String authType) {
//                }
//            } 
//        }; 

        // Install the all-trusting trust manager
        //try {
//            SSLContext sc = SSLContext.getInstance("SSL"); 
//            sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//        } catch (GeneralSecurityException e) {
//            
//        } 
        logger.info("Sending 'POST' request to URL : " + url);
        URL obj = new URL(url);
        //HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setReadTimeout(30000);

        // optional default is GET
        con.setRequestMethod("POST");
//        con.setHostnameVerifier(new HostnameVerifier()
//        {      
//            public boolean verify(String hostname, SSLSession session)
//            {
//                return true;
//            }
//        });

        //add request header
        //con.setRequestProperty("User-Agent", USER_AGENT);
        //String AUTH_STRING = Base64.encodeBase64URLSafeString("cb4d0291-07d4-47df-b196-0b0291cf316e:6d4d284c-3168-49ad-be62-a0daf22b9cca".getBytes());
//        con.setRequestProperty("Authorization", "Bearer " + jwt);
//        con.setRequestProperty("Accept", "application/json");
//        con.setRequestProperty("Content-Type", "application/json");
//        JSONObject jsParams = new JSONObject(params);
//        if (jsParams.has("requestHeader"))
//            if (jsParams.getJSONObject("requestHeader").has("transactionId"))
//                con.setRequestProperty("transactionId", jsParams.getJSONObject("requestHeader").getString("transactionId"));

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(params);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        logger.info("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
           response.append(inputLine);
        }
        in.close();

        //print result
        logger.info("result:" + response.toString());
        
        return response.toString();
    }

    @Override
    public void run() {
        try {
            send();
        }
        catch (Exception e) {
            logger.severe("Error occurred on send(): " + e.getMessage());
        }
    }
    
}
