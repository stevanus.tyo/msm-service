/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import com.bsm.Account;
import com.bsm.Customer;
import com.bsm.FundTransfer;
import com.bsm.TWS;
import java.sql.Statement;
import java.util.Arrays;
import javax.xml.ws.WebServiceException;
import org.json.me.JSONObject;
import t24webservicesimpl.SuccessIndicator;

/**
 *
 * @author Mukhlis Yani
 */
public class AOInquiry {
    protected Boolean valid_parm;
    protected String response_code;
    protected String response;
    protected String template;
    protected String transaction_id
                     , session_id
                     , date_local
                     , ip_address
                     , customer_id
                     , device_type
                     , msisdn
                     , account
                     , date_time
                     , language;
    protected LibCore libCore;
    
    private String zakat;
    private String amount;
    private String email;
    private String branchCode;
    private String branchName;

    
    public AOInquiry(LibCore _libCore, JSONObject param) throws Exception {
        response_code = "99";
        libCore = _libCore;
        amount = param.getString("amount");
        session_id  = param.getString("session_id");
        date_local  = param.getString("date_local");
        date_time  = param.getString("date_time");
        transaction_id  = param.getString("transaction_id");
        //transaction_id_ref  = param.getString("transaction_id_ref");
        ip_address  = param.getString("ip_address");
        customer_id  = param.getString("customer_id");
        device_type  = param.getString("device_type");
        msisdn  = param.getString("msisdn");
        account  = param.getString("account");
        language  = param.getString("language");
        email = param.getString("email");
        branchCode  = param.getString("branch_code");
        branchName  = param.getString("branch_name");
        zakat = param.getString("zakat");
    }
    
    public JSONObject aoInquiryMabrur() throws Exception {
        return null;
    }
    
    public JSONObject aoInquiryBSM() throws Exception {
        JSONObject result;
        JSONObject param;
        String msg = null;
        valid_parm = Boolean.TRUE;
        
        if (valid_parm) {
          EL.trx(LibConfig.application_id, LibFunction.getClassInfo(),
                  session_id + ",AOINQ,"
                  + LibFunction.DateFormat("yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss", date_local) + ",," + ip_address + "," + msisdn + ","
                  + customer_id + ",0," + LibFunction.getDatetime("HH:mm:ss") + "," + device_type, false);

          // Retrieve Accont Info
          param = new JSONObject();
          param.put("inq_timeout", LibConfig.inquiry_timeout_mubp);
          param.put("username_tws", LibConfig.username_tws);
          param.put("password_tws", LibConfig.password_tws);
          param.put("account", account);

          JSONObject inqRes = new JSONObject();
          inqRes.put("branch_code", branchCode);
          inqRes.put("branch_name", branchName);

          LibFunction.setLogMessage("Account Inquiry request: " + libCore.genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));
         
          TWS.configure(LibConfig.loc_tws);

          Account accService = new Account();
          //JSONObject accInqRes = accService.retrieve(param);
          JSONObject accInqRes = accService.retrieve1(param);
          inqRes.put("account", accInqRes);
          
          //LibFunction.setLogMessage("Payment UBP Inquiry response: " + inqRes);
          LibFunction.setLogMessage("Account Inquiry response: " + libCore.genPCIDSSLogData(accInqRes, new String[] {"username_tws","password_tws"}));

            //if (!accInqRes.has("empty")) {
            if (SuccessIndicator.SUCCESS.toString().equals(accInqRes.getString("responsecode")) && !accInqRes.has("empty")) {
                param.put("cif", accInqRes.getString("cif"));

                LibFunction.setLogMessage("Customer Inquiry request: " + libCore.genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));
                
                Customer customerService = new Customer();
                JSONObject custInqRes = customerService.retrieve(param);
                inqRes.put("customer", custInqRes);
                
                LibFunction.setLogMessage("Customer Inquiry response: " + libCore.genPCIDSSLogData(custInqRes, new String[] {"username_tws","password_tws"}));

                if (SuccessIndicator.SUCCESS.toString().equals(custInqRes.getString("responsecode"))) {
                    response_code = "00";
                    response = libCore.GetLanguage("400077");
                    response = response.replace("<br>", "\n");
                    response = response.replace("[cif]", inqRes.getJSONObject("account").getString("cif"));
                    response = response.replace("[name]", inqRes.getJSONObject("customer").getString("shortname"));
                    response = response.replace("[address]", inqRes.getJSONObject("customer").getString("street"));
                    response = response.replace("[phoneno]", msisdn);
                    String deposit = LibFunction.NumberFormat(amount, 0);
                    response = response.replace("[deposit]", deposit);
                    response = response.replace("[email]", email);
                    //response = response.replace("[branch_name]", branchName);
                    //response = response.replace("[branch_name]", inqRes.getJSONObject("account").getString("cocodename"));
                    if (language.equals("id")) {
                        if ("Y".equalsIgnoreCase(zakat))
                            zakat = "Ya";
                        else
                            zakat = "Tidak";
                    }
                    else {
                        if ("Y".equalsIgnoreCase(zakat))
                            zakat = "Yes";
                        else
                            zakat = "No";
                    }
                    response = response.replace("[zakat]", zakat);
                }
                else {
                    response = libCore.GetLanguage("600002");
                }
            } else {
                //response = ErrorMessage();
                //response = inqRes.getJSONObject("soaheader").getString("responsemsg");
                response = libCore.GetLanguage("600002");
            }

            //InsertLogOut(param.toString(), inqRes.toString(), "1", transaction_id, "", inqRes.toString());
            libCore.InsertLogOut(libCore.genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}), inqRes.toString(), "1", transaction_id, "", inqRes.toString());
        }
        else {
            LibFunction.setLogMessage("Invalid parameter: " + msg);
            response = libCore.GetLanguage("600002");
        }
        
        result = new JSONObject();
        result.put("response_code", response_code);
        result.put("response", response);
        
        return result;
    }
    
    public JSONObject aoInquiryBSMWadiah() throws Exception {
        JSONObject result;
        JSONObject param;
        String msg = null;
        valid_parm = Boolean.TRUE;
        
        if (valid_parm) {
          EL.trx(LibConfig.application_id, LibFunction.getClassInfo(),
                  session_id + ",AOINQ,"
                  + LibFunction.DateFormat("yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss", date_local) + ",," + ip_address + "," + msisdn + ","
                  + customer_id + ",0," + LibFunction.getDatetime("HH:mm:ss") + "," + device_type, false);

          // Retrieve Accont Info
          param = new JSONObject();
          param.put("inq_timeout", LibConfig.inquiry_timeout_mubp);
          param.put("username_tws", LibConfig.username_tws);
          param.put("password_tws", LibConfig.password_tws);
          param.put("account", account);

          JSONObject inqRes = new JSONObject();
          inqRes.put("branch_code", branchCode);
          inqRes.put("branch_name", branchName);

          LibFunction.setLogMessage("Account Inquiry request: " + libCore.genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));
         
          TWS.configure(LibConfig.loc_tws);

          Account accService = new Account();
          //JSONObject accInqRes = accService.retrieve(param);
          JSONObject accInqRes = accService.retrieve1(param);
          inqRes.put("account", accInqRes);
          
          //LibFunction.setLogMessage("Payment UBP Inquiry response: " + inqRes);
          LibFunction.setLogMessage("Account Inquiry response: " + libCore.genPCIDSSLogData(accInqRes, new String[] {"username_tws","password_tws"}));

            //if (!accInqRes.has("empty")) {
            if (SuccessIndicator.SUCCESS.toString().equals(accInqRes.getString("responsecode")) && !accInqRes.has("empty")) {
                param.put("cif", accInqRes.getString("cif"));

                LibFunction.setLogMessage("Customer Inquiry request: " + libCore.genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));
                
                Customer customerService = new Customer();
                JSONObject custInqRes = customerService.retrieve(param);
                inqRes.put("customer", custInqRes);
                
                LibFunction.setLogMessage("Customer Inquiry response: " + libCore.genPCIDSSLogData(custInqRes, new String[] {"username_tws","password_tws"}));

                if (SuccessIndicator.SUCCESS.toString().equals(custInqRes.getString("responsecode"))) {
                    response_code = "00";
                    response = libCore.GetLanguage("400136");
                    response = response.replace("<br>", "\n");
                    response = response.replace("[cif]", inqRes.getJSONObject("account").getString("cif"));
                    response = response.replace("[name]", inqRes.getJSONObject("customer").getString("shortname"));
                    response = response.replace("[address]", inqRes.getJSONObject("customer").getString("street"));
                    response = response.replace("[phoneno]", msisdn);
                    String deposit = LibFunction.NumberFormat(amount, 0);
                    response = response.replace("[deposit]", deposit);
                    response = response.replace("[email]", email);
                    //response = response.replace("[branch_name]", branchName);
                    //response = response.replace("[branch_name]", inqRes.getJSONObject("account").getString("cocodename"));
                    /*if (language.equals("id")) {
                        if ("Y".equalsIgnoreCase(zakat))
                            zakat = "Ya";
                        else
                            zakat = "Tidak";
                    }
                    else {
                        if ("Y".equalsIgnoreCase(zakat))
                            zakat = "Yes";
                        else
                            zakat = "No";
                    }
                    response = response.replace("[zakat]", zakat);*/
                }
                else {
                    response = libCore.GetLanguage("600002");
                }
            } else {
                //response = ErrorMessage();
                //response = inqRes.getJSONObject("soaheader").getString("responsemsg");
                response = libCore.GetLanguage("600002");
            }

            //InsertLogOut(param.toString(), inqRes.toString(), "1", transaction_id, "", inqRes.toString());
            libCore.InsertLogOut(libCore.genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}), inqRes.toString(), "1", transaction_id, "", inqRes.toString());
        }
        else {
            LibFunction.setLogMessage("Invalid parameter: " + msg);
            response = libCore.GetLanguage("600002");
        }
        
        result = new JSONObject();
        result.put("response_code", response_code);
        result.put("response", response);
        
        return result;
    }
    
}
