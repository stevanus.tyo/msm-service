/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.json.me.JSONObject;

/**
 *
 * @author Mukhlis Yani
 */
public class TrxLimit {
   
    public static Integer INSERT_LT = 0;
    public static Integer UPDATE_LT = 1;
    
    public static Integer TRANSFER = 0;
    public static Integer TRANSFER_ONLINE = 3;
    public static Integer TRANSFER_SKN = 4;
    public static Integer TRANSFER_RTGS = 5;
    public static Integer TRANSFER_CASH = 6;
    public static Integer CW = 7;
    public static Integer EMONEY = 8;
    public static Integer PURCHASE = 1;
    public static Integer PAYMENT = 2;
    
    private LibQuery mssql;
    
    public TrxLimit(LibQuery mssql) {
        this.mssql = mssql;
    }
    
    public String checkLimit(String msisdn, Integer customerType, Integer trxType, Long trxAmount, JSONObject value) throws Exception {
        String result = "99";

        PreparedStatement ps = null;
        ResultSet rs = null;

        Long trxAmtLimit = (long) 0;
        Long dailyAmtLimit = (long) 0;
        try {
            // Get the limit definition
            ps = mssql.getConn().prepareStatement("select * from mb_limit where customer_type=? and trx_type=? and enabled=1", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ps.setObject(1, customerType);
            ps.setObject(2, trxType);
            rs = ps.executeQuery();
            if (rs.next()) {
                trxAmtLimit = rs.getLong("trx_amount_limit");
                dailyAmtLimit = rs.getLong("daily_amount_limit");
                result = "00";
            }
            else {
                result = "01";
            }
            rs.close();
            ps.close();
            
            if ("00".equals(result)) {
                // Get the trx tracking
                Calendar calTrxDate = Calendar.getInstance();
                Date trxDate = calTrxDate.getTime();
                Long lastAmount;
                Integer action;

                ps = mssql.getConn().prepareStatement("select * from mb_limit_tracking where msisdn=? and trx_type=?", ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ps.setObject(1, msisdn);
                ps.setObject(2, trxType);
                rs = ps.executeQuery();
                if (rs.next()) {
                    Calendar calLastTrxDate = Calendar.getInstance();
                    calLastTrxDate.setTime((Date) rs.getObject("last_trx_date"));
                    if (calLastTrxDate.get(Calendar.DATE) == calTrxDate.get(Calendar.DATE) && 
                        calLastTrxDate.get(Calendar.MONTH) == calTrxDate.get(Calendar.MONTH) &&
                        calLastTrxDate.get(Calendar.YEAR) == calTrxDate.get(Calendar.YEAR)) {
                        lastAmount = rs.getLong("total_amount");
                    }
                    else {
                        lastAmount = (long) 0;
                    }                    
                    action = UPDATE_LT;
                }
                else {
                    lastAmount = (long) 0;
                    action = INSERT_LT;
                }

                if (trxAmtLimit > 0) {
                    if (trxAmount <= trxAmtLimit) {
                        result = "00";
                    }
                    else {
                        result = "02";
                    }
                }
                else {
                    result = "00";
                }

                if ("00".equals(result)) {
                    if (dailyAmtLimit > 0) {
                        if (trxAmount + lastAmount <= dailyAmtLimit) {
                            result = "00";
                        }
                        else {
                            result = "02";
                        }
                    }
                    else {
                        result = "00";
                    }
                }
                
                if ("00".equals(result)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    value.put("trxdate", sdf.format(trxDate));
                    value.put("lastamount", lastAmount);
                    value.put("action", action);
                }
            }
        }
//        catch (Exception e) {
//        }
        finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
        }
       
        return result;
    }

    public void updateLimitTracking(String msisdn, Integer trxType, Long trxAmount, Long lastAmount, Date trxDate, Integer action) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            int i = 0;
            if (action == INSERT_LT) {
                ps = mssql.getConn().prepareStatement("insert into mb_limit_tracking(msisdn, trx_type, last_trx_date, total_amount) values(?, ?, ?, ?)");
                ps.setObject(++i, msisdn);
                ps.setObject(++i, trxType);
                ps.setObject(++i, new Timestamp(trxDate.getTime()));
                ps.setObject(++i, trxAmount + lastAmount);
            }
            else
            if (action == UPDATE_LT) {
                ps = mssql.getConn().prepareStatement("update mb_limit_tracking set last_trx_date=?, total_amount=? where msisdn=? and trx_type=?");
                ps.setObject(++i, new Timestamp(trxDate.getTime()));
                ps.setObject(++i, trxAmount + lastAmount);
                ps.setObject(++i, msisdn);
                ps.setObject(++i, trxType);
            }
            int result = ps.executeUpdate();
        }
//        catch (Exception e) {
//            
//        }
        finally {
            if (rs != null)
                rs.close();
            if (ps != null)
                ps.close();
        }
    }

}
