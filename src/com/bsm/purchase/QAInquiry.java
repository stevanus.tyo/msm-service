/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bsm.purchase;

import java.sql.ResultSet;
import java.sql.Statement;
import org.json.me.JSONObject;
import pkg_lib.LibCore;
import pkg_lib.LibFunction;
import pkg_lib.LibMSSQL;
import pkg_lib.Setting;

/**
 *
 * @author Mukhlis Yani
 */
public class QAInquiry {
    
    protected Boolean valid_parm;
    protected String response_code;
    protected String response;
    protected String template;
    protected String transaction_id, transaction_id_ref, session_id, date_local, amount,
                     ip_address, customer_id, device_type, msisdn, account, date_time, language, email, supplier_id;
    protected LibCore libCore;
    protected LibMSSQL mssqlq;
    protected Statement stmt;
    protected ResultSet getResultSet;
    protected JSONObject params;
    
    private String service_code;
    private String onBehalf;
    private String reportEmail;
    
    //private String zakat;
    //private String purpose_id;
    //private String eventCode;
//    private String qrCode;
//    private String qrType;
//    private String DE0, zpk, DE7, DE11, DE12, DE13, DE49, DE52;
    
    //private JSONObject inqResp;
    
    //private static String idCMMsg = "Cek mutasi Anda atau hubungi BSM Call 14040";
    //private static String enCMMsg = "Check your mutation or contact BSM Call 14040";
    //private static String idPUATMMMsg = "Silahkan datang ke cabang BSM terdekat untuk mengambil kartu ATM Anda";
    //private static String enPUATMMsg = "Please visit near by branch to pick up your ATM card";
    
    public QAInquiry(LibCore _libCore, LibMSSQL mssql, JSONObject params) throws Exception {
        response_code = "99";
        libCore = _libCore;
        mssqlq = mssql;

        this.params = params;
        session_id  = params.getString("session_id");
        date_local  = params.getString("date_local");
        //date_time  = params.getString("date_time");
        transaction_id  = params.getString("transaction_id");
        //transaction_id_ref  = params.getString("transaction_id_ref");
        ip_address  = params.getString("ip_address");
        customer_id  = params.getString("customer_id");
        device_type  = params.getString("device_type");
        msisdn  = params.getString("msisdn");
        account  = params.getString("srcaccno");
        language  = params.getString("language");
        email  = params.getString("email");
        service_code  = params.getString("service_code");
        
        supplier_id = params.getString("supplier_id");
        amount = params.getString("amount");
        onBehalf = params.getString("onbehalf");
        reportEmail = params.getString("report_email");

        /*amount = param.getString("amount");
        zakat  = param.getString("zakat");
        purpose_id = param.getString("purpose_id");
        eventCode = param.getString("eventcode");
        inqResp = param.getJSONObject("inqResp");*/
    }

    public JSONObject execute() throws Exception {
        JSONObject result;

        valid_parm = true;
        if (valid_parm) {
            JSONObject inqReq = null;
            JSONObject inqResp = null;

            inqReq = params;

            QA qa = new QA(libCore, mssqlq, params);
            //String accountNo = qa.getSupplier(Integer.valueOf(supplier_id), "accountno");
            JSONObject supplier = qa.getSupplier(Integer.valueOf(supplier_id));
            JSONObject item = qa.getItem(supplier_id, Integer.valueOf(amount));
            String fee = Setting.getProperty("qa.fee", true);
            String tot_amt = String.valueOf(Long.parseLong(amount) + Long.parseLong(fee));

            inqResp = new JSONObject(params.toString());
            inqResp.put("destaccno", supplier.getString("accountno"));
            inqResp.put("supp_name", supplier.getString("name"));
            inqResp.put("supp_email", supplier.getString("email"));
            inqResp.put("item_name", item.getString("description"));
            inqResp.put("on_behalf", onBehalf);
            inqResp.put("report_email", reportEmail);
            inqResp.put("code_name", "Hewan Kurban");
            inqResp.put("fee", fee);
            inqResp.put("tot_amt", tot_amt);
            
            response_code = "00";
            response = libCore.GetLanguage("400140");
            response = response.replace("<br>", "\n");
            response = response.replace("[code_name]", inqResp.getString("code_name"));
            response = response.replace("[supp_name]", supplier.getString("name"));
            response = response.replace("[item_name]", item.getString("description"));
            response = response.replace("[on_behalf]", inqResp.getString("on_behalf"));
            response = response.replace("[report_email]", inqResp.getString("report_email"));
            response = response.replace("[amount]", LibFunction.NumberFormat(amount));
            response = response.replace("[fee]", LibFunction.NumberFormat(fee));
            response = response.replace("[tot_amt]", LibFunction.NumberFormat(tot_amt));
            
            libCore.setResponseCode(response_code);
            libCore.InsertLogOut(inqReq.toString(), inqResp.toString(), "1", transaction_id, "", inqResp.toString());

        }
        else {
            response = libCore.GetLanguage("600002");
        }

        result = new JSONObject();
        result.put("response_code", response_code);
        result.put("response", response);

        return result;
    }

}