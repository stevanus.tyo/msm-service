/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bsm.purchase;

import com.bsm.Account;
import com.bsm.FTCharge;
import com.bsm.TWS;
import java.net.SocketTimeoutException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import org.json.me.JSONObject;
import pkg_lib.EL;
import pkg_lib.LibConfig;
import pkg_lib.LibCore;
import static pkg_lib.LibCore.genPCIDSSLogData;
import pkg_lib.LibFunction;
import pkg_lib.LibMSSQL;
import pkg_lib.TrxLimit;
import t24webservicesimpl.SuccessIndicator;

/**
 *
 * @author Mukhlis Yani
 */
public class QAPayment {
    
    protected Boolean valid_parm;
    protected String response_code;
    protected String response;
    protected String template;
    protected String transaction_id, transaction_id_ref, session_id, date_local, amount,
                     ip_address, customer_id, device_type, msisdn, account, date_time, language, email, supplier_id;
    protected LibCore libCore;
    protected LibMSSQL mssqlq;
    protected Statement stmt;
    protected Statement stmt_q;
    protected ResultSet getResultSet;
    protected JSONObject params;
    
    private String service_code;
    //private String zakat;
    //private String purpose_id;
    //private String eventCode;
//    private String qrCode;
//    private String qrType;
//    private String DE0, zpk, DE7, DE11, DE12, DE13, DE49, DE52;
    
    //private JSONObject inqResp;
    
    //private static String idCMMsg = "Cek mutasi Anda atau hubungi BSM Call 14040";
    //private static String enCMMsg = "Check your mutation or contact BSM Call 14040";
    //private static String idPUATMMMsg = "Silahkan datang ke cabang BSM terdekat untuk mengambil kartu ATM Anda";
    //private static String enPUATMMsg = "Please visit near by branch to pick up your ATM card";
    
    public QAPayment(LibCore _libCore, LibMSSQL mssql, JSONObject params) throws Exception {
        response_code = "99";
        libCore = _libCore;
        mssqlq = mssql;

        this.params = params;
        session_id  = params.getString("session_id");
        date_local  = params.getString("date_local");
        date_time  = params.getString("date_time");
        transaction_id  = params.getString("transaction_id");
        transaction_id_ref  = params.getString("transaction_id_ref");
        ip_address  = params.getString("ip_address");
        customer_id  = params.getString("customer_id");
        device_type  = params.getString("device_type");
        msisdn  = params.getString("msisdn");
        account  = params.getString("srcaccno");
        language  = params.getString("language");
        email  = params.getString("email");
        service_code  = params.getString("service_code");
        
        /*amount = param.getString("amount");
        zakat  = param.getString("zakat");
        purpose_id = param.getString("purpose_id");
        eventCode = param.getString("eventcode");
        inqResp = param.getJSONObject("inqResp");*/
    }

    public JSONObject execute() throws Exception {
        JSONObject result;
        JSONObject inqReq = null;
        JSONObject inqResp = null;

        stmt = mssqlq.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        mssqlq.setQuery("SELECT CAST(request AS TEXT) AS request, CAST(response AS TEXT) AS response"
                        + " FROM MB_LogOut with (NOLOCK) where transaction_id='" + transaction_id + "' AND status='1'");
        getResultSet = stmt.executeQuery(mssqlq.getQuery());
        getResultSet.last();
        if (getResultSet.getRow() > 0) {
          getResultSet.beforeFirst();
          while (getResultSet.next()) {

            stmt_q = mssqlq.getConn().createStatement();
            String sql = "UPDATE MB_LogOut set status='2', transaction_id_ref='" + transaction_id_ref + "' "
                         + "WHERE transaction_id='" + transaction_id + "'";
            stmt_q.executeUpdate(sql);

            inqReq = new JSONObject(getResultSet.getString("request"));
            inqResp = new JSONObject(getResultSet.getString("response"));
          }
        } 
        else {
            throw new Exception("Previous inquiry data can not be found: transaction_id: " + transaction_id);
        }
        getResultSet.close();
        stmt.close();
        
        // Check Limit      
        /*if (!libCore.checkLimit(TrxLimit.PURCHASE)) {
          response_code = libCore.getResponseCode();
          response = libCore.getResponse();
          
          result = new JSONObject();
          result.put("response_code", response_code);
          result.put("response", response);
          
          return result;
        }*/

        JSONObject param = new JSONObject();
        param.put("msisdn" , msisdn);
        param.put("customer_id" , customer_id);
        param.put("session_id" , session_id);
        param.put("inq_timeout", LibConfig.inquiry_timeout_mubp);
        param.put("pymt_timeout", LibConfig.payment_timeout_mubp);
        param.put("username_tws", LibConfig.username_tws);
        param.put("password_tws", LibConfig.password_tws);
        param.put("companyid", "ID0010001");
        param.put("sourceacc", inqResp.getString("srcaccno").toUpperCase());
        param.put("account", inqResp.getString("srcaccno").toUpperCase());
        param.put("destacc", inqResp.getString("destaccno").toUpperCase());
        param.put("amount", inqResp.getString("amount"));
        param.put("paymentdetail", inqResp.getString("on_behalf") + "-" + inqResp.getString("supp_name"));
        param.put("messageid", transaction_id_ref);
        param.put("chgacc", account);
        //param.put("chgtype", "FTCHGQRPAY");
        param.put("chgtype", "FTCHGQUR");
        long admin_fee = inqResp.getLong("fee");
        if (admin_fee > 0)
            param.put("chgamount", "IDR" + admin_fee);
        else
            param.put("chgamount", "IDR0.00");
        
        TWS.configure(LibConfig.loc_tws);
        
        // Check Account Status dan Balance
        
        Account acc = new Account();
        JSONObject accInfo = acc.retrieve1(param);
        
        if ("".equals(accInfo.getString("postingrestrict"))) {
            double dblAmount = Double.parseDouble(inqResp.getString("amount"));
            double dblAvailBal = Double.parseDouble(accInfo.getString("availbal").replace(",", ""));
            
            if (dblAvailBal >= dblAmount) {
        libCore.InsertLogOut(genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}), "", "2", transaction_id_ref, transaction_id, "");

        EL.trx(LibConfig.application_id, LibFunction.getClassInfo(),
                session_id + ",PURQA,"
                + LibFunction.DateFormat("yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss", date_local) + ",," + ip_address + "," + msisdn + ","
                + customer_id + ",0," + LibFunction.getDatetime("HH:mm:ss") + "," + device_type, false);

        LibFunction.setLogMessage("Purchase QA request: " + genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));

        JSONObject finalResp = new JSONObject();
        Boolean isReversal = false;
        Boolean isStep1Done;

                //TWS.configure(LibConfig.loc_tws);

        FTCharge ftCharge = new FTCharge();
        JSONObject ftcResp = null;
        try {
            ftcResp = ftCharge.execute1(param);
        }
        catch (Exception e) {
            Throwable t = TWS.getRootException(e);
            LibFunction.setLogMessage("App Error: " + t.toString() + "\n"
                      + Arrays.toString(t.getStackTrace()).replace("),", "),\n"));
              EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 121, EL.elCritical,
                      "App Error: " + t.toString());

            isReversal = true;
            //isStep1Done = true;

            //System.out.println(t.getMessage());
            if (t instanceof SocketTimeoutException) {
              isStep1Done = true;
              response_code = "68";
              libCore.setResponseCode(response_code);
              response = libCore.ErrorMessage();
//              setHtppResponse();
//              return;
            }
            else {
                response_code = "99";
                response = libCore.GetLanguage("600002");
            }
        }
        
        finalResp.put("ft", ftcResp);

        LibFunction.setLogMessage("Purchase QA response: " + genPCIDSSLogData(ftcResp, new String[] {"username_tws","password_tws"}));

        if (!isReversal) {
          if (SuccessIndicator.SUCCESS.toString().equals(ftcResp.getString("responsecode"))) {
            response_code = "00";
            String code_name = inqResp.getString("code_name");
            String trans_ref = ftcResp.getString("trxid");
            String struck_no = transaction_id_ref;
            String maskedMSISDN = LibFunction.genMaskedMSISDN(msisdn);
                    String footerMsg = libCore.GetLanguage("600090");

            String status = "BERHASIL";

            template = LibFunction.ReadFile(LibConfig.template_path + "qa_purchase.html");
            template = template.replace("{status}", status);
            template = template.replace("{image}", LibConfig.image_path);
            template = template.replace("{trans_ref}", trans_ref);
            template = template.replace("{terminal}", maskedMSISDN);
            template = template.replace("{struck}", struck_no); //suro kembali ke asal. struck beda isinya sama stan
            template = template.replace("{date_time}", date_time);
            template = template.replace("{code_name}", code_name);
            template = template.replace("{supp_name}", inqResp.getString("supp_name"));
            template = template.replace("{item_name}", inqResp.getString("item_name"));
                    template = template.replace("{on_behalf}", inqResp.getString("on_behalf"));
                    template = template.replace("{report_email}", inqResp.getString("report_email"));
            template = template.replace("{amount}", LibFunction.NumberFormat(inqResp.getString("amount")));
            template = template.replace("{fee}", LibFunction.NumberFormat(inqResp.getString("fee")));
            template = template.replace("{tot_amt}", LibFunction.NumberFormat(inqResp.getString("tot_amt")));
            template = template.replace("{accno}", inqResp.getString("srcaccno"));
                    template = template.replace("{footer}", footerMsg);
            
            response = libCore.GetLanguage("400141");
            response = response.replace("[trans_ref]", trans_ref);
            response = response.replace("[date_time]", date_time);
            response = response.replace("[struck]", struck_no);
            response = response.replace("[terminal]", maskedMSISDN);
            response = response.replace("[accno]", inqResp.getString("srcaccno"));
            response = response.replace("[supp_name]", inqResp.getString("supp_name"));
            response = response.replace("[item_name]", inqResp.getString("item_name"));
            response = response.replace("[on_behalf]", inqResp.getString("on_behalf"));
                    response = response.replace("[report_email]", inqResp.getString("report_email"));
            response = response.replace("[amount]", LibFunction.NumberFormat(inqResp.getString("amount")));
            response = response.replace("[fee]", LibFunction.NumberFormat(inqResp.getString("fee")));
            response = response.replace("[tot_amt]", LibFunction.NumberFormat(inqResp.getString("tot_amt")));

            JSONObject resp = new JSONObject();
            resp.put("msg", response);
            resp.put("trxref", trans_ref);
            resp.put("date_time", date_time);
            resp.put("title", "Pembelian " + code_name);
                    //resp.put("footer_msg", libCore.GetLanguage("600090") + "\n\n" + libCore.GetLanguage("400062"));
                    //resp.put("footer_msg", libCore.GetLanguage("600090"));
                    resp.put("footer_msg", footerMsg);

            response = resp.toString();

                    if (inqResp.has("supp_email") &&!"".equals(inqResp.getString("supp_email")))
                        if ("".equals(email))
                            email = inqResp.getString("supp_email");
                        else
                        email += "," + inqResp.getString("supp_email");
            LibFunction.sendEmailAsync(trans_ref, email, "Transaksi Bank Syariah Mandiri (" + code_name + ")",
                    template, template, false);

                    //libCore.updateLimitTracking(TrxLimit.PURCHASE);
          }
          else {
              response_code = "99";
              //response = GetLanguage("600002");
              response = TWS.getErrorMessage(language, ftcResp.getString("responsemsg"));
              if (response == null)
                  response = libCore.GetLanguage("600002");
          }
        }

        stmt_q = mssqlq.getConn().createStatement();
        String sql = "UPDATE MB_LogOut set response_code='" + response_code
                + "', request='" + libCore.escapeString(param.toString()) + "', "
                + "response='" + libCore.escapeString(finalResp.toString()) + "', "
                + "status='2', transaction_id_ref='" + transaction_id + "' "
                + "WHERE transaction_id='" + transaction_id_ref + "'";
        stmt_q.executeUpdate(sql);
            }
            else {
                libCore.setResponseCode("51");
                response = libCore.ErrorMessage();
                LibFunction.setLogMessage(response + ": " + genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));
            }
        }
        else {
            response = libCore.GetLanguage("400100");
            LibFunction.setLogMessage(response + ": " + genPCIDSSLogData(param, new String[] {"username_tws","password_tws"}));
        }

        result = new JSONObject();
        result.put("response_code", response_code);
        result.put("response", response);

        return result;
    }

}