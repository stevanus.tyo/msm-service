/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bsm.purchase;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Arrays;
import org.json.me.JSONArray;
import org.json.me.JSONObject;
import pkg_lib.EL;
import pkg_lib.LibConfig;
import pkg_lib.LibCore;
import pkg_lib.LibFunction;
import pkg_lib.LibMSSQL;

/**
 *
 * @author Mukhlis Yani
 */
public class QA {
    
    protected Boolean valid_parm;
    protected String response_code;
    protected String response;
//    protected String template;
//    protected String transaction_id, transaction_id_ref, session_id, date_local, 
//                     ip_address, customer_id, device_type, msisdn, account, date_time, language, email;
    protected LibCore libCore;
    protected LibMSSQL mssqlq;
    protected Statement stmt;
    protected ResultSet getResultSet;
    
    private String service_code;
    //private String zakat;
    //private String purpose_id;
    //private String eventCode;
//    private String qrCode;
//    private String qrType;
//    private String DE0, zpk, DE7, DE11, DE12, DE13, DE49, DE52;
    
    //private JSONObject inqResp;
    
    //private static String idCMMsg = "Cek mutasi Anda atau hubungi BSM Call 14040";
    //private static String enCMMsg = "Check your mutation or contact BSM Call 14040";
    //private static String idPUATMMMsg = "Silahkan datang ke cabang BSM terdekat untuk mengambil kartu ATM Anda";
    //private static String enPUATMMsg = "Please visit near by branch to pick up your ATM card";
    
    public QA(LibCore _libCore, LibMSSQL mssql, JSONObject params) throws Exception {
        response_code = "99";
        libCore = _libCore;
        mssqlq = mssql;
        
//        transaction_id  = params.getString("transaction_id");
//        qrCode = params.getString("qrcode");
//        qrType = params.getString("qrtype");
//        DE0 = params.getString("DE0");
//        zpk = params.getString("ZPK");
//        DE7 = params.getString("DE7");
//        DE11 = params.getString("DE11");
//        DE12 = params.getString("DE12");
//        DE13 = params.getString("DE13");
//        DE49 = params.getString("DE49");
//        DE52 = params.getString("DE52");
        
        /*session_id  = param.getString("session_id");
        date_local  = param.getString("date_local");
        date_time  = param.getString("date_time");
        transaction_id  = param.getString("transaction_id");
        transaction_id_ref  = param.getString("transaction_id_ref");
        ip_address  = param.getString("ip_address");
        customer_id  = param.getString("customer_id");
        device_type  = param.getString("device_type");
        msisdn  = param.getString("msisdn");
        account  = param.getString("account");
        language  = param.getString("language");
        email  = param.getString("email");
        service_code  = param.getString("service_code");

        amount = param.getString("amount");
        zakat  = param.getString("zakat");
        purpose_id = param.getString("purpose_id");
        eventCode = param.getString("eventcode");
        inqResp = param.getJSONObject("inqResp");*/
    }
    
    public JSONArray getListSupplier() {
        JSONArray arr_json_data = new JSONArray();
        try {
          int x = 0;
          stmt = mssqlq.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
          mssqlq.setQuery("SELECT * FROM mb_supplierhk with (NOLOCK) where isenabled=1 "
                  + "order by id, name ASC");
          getResultSet = stmt.executeQuery(mssqlq.getQuery());
          getResultSet.last();
          if (getResultSet.getRow() > 0) {
            getResultSet.beforeFirst();
            while (getResultSet.next()) {
              JSONObject json_data1 = new JSONObject();
              //json_data1.put("supplier_id", getResultSet.getString("id"));
              json_data1.put("code", getResultSet.getString("id"));
              json_data1.put("name", getResultSet.getString("name"));
              json_data1.put("description", getResultSet.getString("description"));
              arr_json_data.put(x, json_data1);
              x++;
            }
          }
          getResultSet.close();
          stmt.close();
        } 
        catch (Exception Ex) {
          response_code = "99";
          response = libCore.GetLanguage("600002");
          LibFunction.setLogMessage("App Error: " + Ex.toString() + "\n"
                  + Arrays.toString(Ex.getStackTrace()).replace("),", "),\n"));
          EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 73,
                  EL.elCritical, "App Error: " + Ex.toString());
        }
        
        return arr_json_data;
        //return null;
    }

    public JSONObject getSupplier(Integer id) {
        JSONObject result = null;
        try {
          int x = 0;
          stmt = mssqlq.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
          mssqlq.setQuery("SELECT * FROM mb_supplierhk with (NOLOCK) where isenabled=1 "
                  + "and id=" + id
                  + "order by id, name ASC");
          getResultSet = stmt.executeQuery(mssqlq.getQuery());
          getResultSet.last();
          if (getResultSet.getRow() > 0) {
            result = new JSONObject();
            getResultSet.beforeFirst();
            ResultSetMetaData rsmd = getResultSet.getMetaData();
            while (getResultSet.next()) {
                for (int i = 0; i < rsmd.getColumnCount(); i++)
                    result.put(rsmd.getColumnLabel(i + 1), getResultSet.getString(i + 1));
            }
          }
          getResultSet.close();
          stmt.close();
        } 
        catch (Exception Ex) {
          response_code = "99";
          response = libCore.GetLanguage("600002");
          LibFunction.setLogMessage("App Error: " + Ex.toString() + "\n"
                  + Arrays.toString(Ex.getStackTrace()).replace("),", "),\n"));
          EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 73,
                  EL.elCritical, "App Error: " + Ex.toString());
        }
        
        return result;
    }
    
    public String getSupplier(Integer id, String fieldName) {
        String result = "";
        try {
          int x = 0;
          stmt = mssqlq.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
          mssqlq.setQuery("SELECT " + fieldName + " FROM mb_supplierhk with (NOLOCK) where isenabled=1 "
                  + "and id=" + id
                  + "order by id, name ASC");
          getResultSet = stmt.executeQuery(mssqlq.getQuery());
          getResultSet.last();
          if (getResultSet.getRow() > 0) {
            getResultSet.beforeFirst();
            while (getResultSet.next()) {
              result = getResultSet.getString(fieldName);
            }
          }
          getResultSet.close();
          stmt.close();
        } 
        catch (Exception Ex) {
          response_code = "99";
          response = libCore.GetLanguage("600002");
          LibFunction.setLogMessage("App Error: " + Ex.toString() + "\n"
                  + Arrays.toString(Ex.getStackTrace()).replace("),", "),\n"));
          EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 73,
                  EL.elCritical, "App Error: " + Ex.toString());
        }
        
        return result;
    }
    
    public JSONObject getItem(String code, Integer id) {
        JSONObject result = null;
        try {
          int x = 0;
          stmt = mssqlq.getConn().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
          mssqlq.setQuery("SELECT * FROM mb_denom with (NOLOCK) where "
                  + "code=" + code + " and id_denom=" + id + " ");
                  //+ "order by id ASC");
          getResultSet = stmt.executeQuery(mssqlq.getQuery());
          getResultSet.last();
          if (getResultSet.getRow() > 0) {
            result = new JSONObject();
            getResultSet.beforeFirst();
            ResultSetMetaData rsmd = getResultSet.getMetaData();
            while (getResultSet.next()) {
                for (int i = 0; i < rsmd.getColumnCount(); i++)
                    result.put(rsmd.getColumnLabel(i + 1), getResultSet.getString(i + 1));
            }
          }
          getResultSet.close();
          stmt.close();
        } 
        catch (Exception Ex) {
          response_code = "99";
          response = libCore.GetLanguage("600002");
          LibFunction.setLogMessage("App Error: " + Ex.toString() + "\n"
                  + Arrays.toString(Ex.getStackTrace()).replace("),", "),\n"));
          EL.log(LibConfig.application_id, LibFunction.getClassInfo(), 73,
                  EL.elCritical, "App Error: " + Ex.toString());
        }
        
        return result;
    }
    
}